<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="jquery.knob.min.js"></script>

		<style>
		#compass {
			height: 40px;
			width: 120px;
			overflow: hidden;
			margin: 47px;
			//box-shadow: inset 0 0 12px white;
		}
		#compass_bg {
			position: relative;
		}
		#shadow_bg {
			position: relative;
			top: -40px;
		}
		#knob_container {
			position: relative;
			top: -156px;
			left: 16px;
			z-index: -42;
			background: #444;
			width: 180px;
			height: 180px;
			border-radius: 90px;
		}
		#compass_knob {
			display: none;
		}
		</style>
	</head>
	<body>
		
		<div>
			Use the knob to simulate heading value...
		</div>
		<div>
			<input class="knob" data-width="150" data-cursor=true data-fgColor="#222222" max=360 data-thickness=.3 value="29">
		</div>
		...or point your mouse on the image below
		<div id="pointer"><img id="pointer_bg" src="compass_bg.png" /></div>
		<p style="margin-bottom: 127px">heading: <span id="heading"></span></p>
		<p> </p>
		<p> </p>
		<div id="compass" style="margin: 47px">
			<img id="compass_bg" src="compass_bg.png" />
		<!--	<img id="shadow_bg" src="shadow.png" /> -->
		</div>
		<div id="knob_container">
			<input id="compass_knob" data-width="180" data-cursor=true data-fgColor="#ff0000" max=360 data-thickness=.3 value="29">
		</div>
			
		<script>
		</script>
		<script>
			$(".knob").knob({
				'min':0,
				'max':360,
				'change' : function (input) { $( "#compass_bg").css( {marginLeft: -2 * input }); $("#compass_knob").val(input).trigger('change'); }
			});
			
			$("#compass_knob").knob({
				'min':0,
				'max':360
			});
			
			$( "#pointer" ).mousemove(function( event ) {
				var heading = (event.pageX - 70)/2 ;  		// 70 offset from document top left corner= 8px for the div + 62 px for the N starting at pixel 72 on the image
				if (heading<0) heading = 360+heading;
				$( "#heading" ).html( heading );
				$('.knob').val(heading).trigger('change');
				$( "#compass_bg").css({marginLeft: -2*heading });
				 $("#compass_knob").val(heading).trigger('change');
			});
		</script>

	</body>
</html>
