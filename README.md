<h3>The website</h3>

<p>PgEarth is a worldwide collaboratice free-flight sites database.</p>

<p>What you shall find on the website :
<ul>
<li>free flight sites : text info, orientations, pictures, maps, etc</li>
<li>free flight clubs and professionalw</li>
<li>no adds, nor any gafam intrusive 'like' buttons</li>
</ul>
</p>

<p>The database beeing collaborative and open, users are more than welcome to add/edit information for any items to keep it as up to date as possible.</p>

<p>You can browse it:
<ul>
<li>here: http://paraglidinearth.com for the current stable version (as of october 2017, in read-only flavor...)</li>
<li>or here: http://paragliding.earth for the beta/testing/dev version which code is shared on this gitlab</li>
</ul>
</p>


<h3>The framagit project</h3>

<p>The website is beeing rebuilt from scratch to be mobile-friendly, responsive, 2.0, bootstraped, googlemaps and fb buttons free, with yet such an amazing user experience, in order to make the world a better place by extensive use of fancy jquery and ajax requests.</p>

<p>If you want to help building this piece of of the web, please join the team !</p>
