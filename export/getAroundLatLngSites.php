<?php

header("Content-Type: text/xml");
include "../connexion.php";

$latref = 3.14159265 * $_GET['lat']   / 180;
$lngref= 3.14159265 * $_GET['lng']   / 180;
$distance_Get = $_GET['distance'];
$count=0;
$style='brief';

if (isset ($_GET['style'] )) $style= $_GET['style'] ; 

$query = "SELECT id, lat, lng FROM site where lat!= 0 and lng != 0";
$result = mysqli_query($bdd, $query);
$where = " id=-42 ";  // a dummy where clause to start the where clause (should be "false")

while ($fetchR = mysqli_fetch_array($result)){     // we try all the sites in the db....

	$latRad = 3.14159265 * $fetchR['lat']   / 180;
	$lngRad = 3.14159265 * $fetchR['lng']   / 180;
	$d = round(6366 * acos(cos($latRad) * cos($latref) * cos($lngref-$lngRad) + sin($latRad) * sin($latref)),2);

	if ($d < $distance_Get){     //.... we keep only the close ones...
		$where .= " OR id = ".$fetchR['id'];
	}
}

$query = "select * from site where $where";
$result = mysqli_query($bdd, $query);

include "generateGPXSitesList.php";


?>
<?php
/*
if ($limit>0) {
	array_multisort( $distance_meter, SORT_ASC,
		$name, $iso, $id_site,
		$lat, $lng, $takeoff_altitude, 
		$landing_lat, $landing_lng, $landing_altitude, 
		$takeoff_description, $landing_description,
		$hike, $soaring, $thermal, $xc ,$flatland,$winch, $hanggliding,  
		$takeoff_comments, $weather, $takeoff_going_there, $takeoff_flight_rules, $takeoff_contacts,
		$web,
		$N,$NE,$E,$SE,$S,$SW,$W,$NW
	);
}
*/
?>
