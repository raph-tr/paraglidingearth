<!DOCTYPE html>

<html>

<head>

<title>Leaflet Web Map</title>

<!-- reference to Leaflet CSS -->
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />

<!-- reference to Leaflet JavaScript -->
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>

 <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
   
<!-- set width and height styles for map -->
<style>
#map {
    width: 960px;
    height:500px;
} 
</style>

</head>

<body>
	
	<a href="#" onclick="addNew('site')">new</a>

    <!-- place holder for map -->
    <div id="map"></div>

<script>
    // create map object, tell it to live in 'map' div and provide center and zoom option values 
    var map = L.map('map',{
    center: [43.64701, 5],
    zoom: 5
    });

    // add base map tiles from OpenStreetMap and attribution info to 'map' div
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
</script>

<script>  
    
function addNew(item){

	var formStr = "<fieldset>";
	formStr += "<div class=\"form-group\">";
	formStr += "    <label for=\"name\" class=\"col-sm-3 control-label\">Name : <\/label>";
	formStr += "    <div class=\"col-sm-9\">";
	formStr += "	  <input type='text' name='name' id='name' value='name' maxlength=\"64\" required placeholder=\"name\" class=\"form-control\"\/>";
	formStr += "    <\/div>";
	formStr += "	<label for=\"iso\" class=\"col-sm-3 control-label\">Country: <\/label>";
	formStr += "    <div class=\"col-sm-9\">";
	formStr += "      <input type='text' name='alt' id='iso' value='0' required placeholder=\"iso\" class=\"form-control\" disabled=\"disabled\"\/>";
	formStr += "    <\/div>";
	formStr += "	<label for=\"alt\" class=\"col-sm-3 control-label\">Altitude : <\/label>";
	formStr += "    <div class=\"col-sm-9\">";
	formStr += "      <input type='text' name='alt' id='alt' value='0' required placeholder=\"alt\" class=\"form-control\" disabled=\"disabled\"\/>";
	formStr += "    <\/div>";
	formStr += "	  <input type='hidden' name='lng' id='lng' value='0' required placeholder=\"lng\" class=\"form-control\"\/>";
	formStr += "	  <input type='hidden' name='lat' id='lat' value='0' required placeholder=\"lat\" class=\"form-control\"\/>";
	formStr += "<\/div>";
	formStr += "<a href=\"#\" id=\"addLanding\">Add landing...<\/a><\/div>";
	formStr += "<\/fieldset>";

	if (map.getZoom() < -6){
		alert('please first zoom in the map\ncloser to the place you want the new '+item+' to be');
	} else {
		marker = L.marker([map.getCenter().lat, map.getCenter().lng], {draggable:true}).addTo(map).bindPopup("Drag me to the exact right place...").openPopup();
		marker.on('dragend', function(){
			marker.bindPopup(formStr).openPopup();
			$("#lng").val(marker.getLatLng().lng);
			$("#lat").val(marker.getLatLng().lat);
			findNearby(marker.getLatLng().lat, marker.getLatLng().lng);
		});
	}
}

$("#addLanding").on("click", function () {
	alert('to do');	
	}
);


function findNearby(lat, lng) {
	
	$.getJSON('http://api.geonames.org/findNearbyJSON?lat='+lat+'&lng='+lng+'&username=raf', function( data ) {
		$("#name").val(data.geonames[0].toponymName);
		$("#iso").val(data.geonames[0].countryCode.toLowerCase());
	});
 
	$.getJSON('http://api.geonames.org/srtm1JSON?lat='+lat+'&lng='+lng+'&username=raf', function( data ) {
		$("#alt").val(data.srtm1);
	});

}
</script>

</body>

</html>
