<?php
include "../../connexion.php";

$latRef = $_GET['lat'];
$lngRef = $_GET['lng'];

$box=0.5;

$X1 = $latRef-$box;
$X2 = $latRef+$box;
$Y1 = $lngRef-$box;
$Y2 = $lngRef+$box;

$q="select id, name, lat, lng, ffvl_suid,
       ( 3959 * acos( cos( radians($latRef) ) 
              * cos( radians( lat ) ) 
              * cos( radians( lng ) - radians($lngRef) ) 
              + sin( radians($latRef) ) 
              * sin( radians( lat ) ) ) ) AS distance 
		from site
		where lat > $X1 and lat < $X2 
		and lng > $Y1 and lng < $Y2
		having distance < 10 ORDER BY distance LIMIT 1;
";

$r = mysqli_query($bdd, $q);

while ( $v = mysqli_fetch_array($r) ) {
	echo '{"id":'.$v['id'].',"name":"'.$v['name'].'","distance":'.round($v['distance'], 3).',"ffvl_suid":'.$v['ffvl_suid'].'}';
}
?>
