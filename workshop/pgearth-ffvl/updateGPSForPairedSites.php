<?php
session_start();
include "../../connexion.php";
$q = "SELECT id, name, lat, lng, ffvl_suid FROM `site` WHERE `iso` LIKE 'fr' and lat=0 and lng=0 and ffvl_suid!=0 ";
$r = mysqli_query($bdd, $q);
?>
<html>
<body>
	<style>
		tr {
			border: 1px solid black;
		}
	</style>
	<div id="loading">loading ffvl json...</div>
	<table id="theTable">
		<thead>
			<tr>
				<td>pge ID</td>
				<td>pge name</td>
				<td>pge lat</td>
				<td>pge lng</td>
				<td>ffvl id</td>
				<td>ffvl name</td>
				<td>ffvl lat</td>
				<td>ffvl lng</td>
				<td>go to</td>
				<td>gps</td>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
<script>
	jQuery.ajaxSetup({async:false});
	var pgeSites = [
<?php
while ( $v = mysqli_fetch_array($r) ) {
?>
	{ id: <?php echo $v['id'];?>, name:"<?php echo addslashes($v['name']);?>", lat: <?php echo $v['lat'];?>, lng: <?php echo $v['lng'];?>, ffvl_suid: <?php echo $v['ffvl_suid'];?> },
<?php
}	
?>
	];
	var ffvlSites = [];
	var p = 0;
	var getFFVLData =	$.getJSON("http://data.ffvl.fr/json/sites.json", function( result ) {
							for (var i = 0; i < result.length; i++) {
								if ( result[i].site_type == "vol" && result[i].numero.substr(2,1)=="D" && result[i].numero.substr(6,1)=="A") {   //  we have a main takeoff !!!
									p++;
									// document.write(result[i].numero+" - "+result[i].nom+"<br/>");
									ffvlSites.push({ id:result[i].suid, name:result[i].nom, lat:result[i].lat, lng:result[i].lon });
								}
							}
						});
					
	getFFVLData.done( function () {

		$("#loading").hide();
		$("#theTable").show();	
		var distance = 100000;
		var match=0;
		
		for (var i = 0; i < pgeSites.length ; i++) {
//			document.write( "<hr />"+pgeSites[i].name+"<br/>");
			var row = '<tr id="row'+pgeSites[i].id+'" ><td>'+pgeSites[i].id+'</td><td>'+pgeSites[i].name+'</td><td>'+pgeSites[i].lat+'</td><td>'+pgeSites[i].lng+'</td>';
			minDistance = 1000000;
			for (var j = 0; j < ffvlSites.length; j++) {
	//			var distance =  ( 3959 * Math.acos( Math.cos( toRadians(ffvlSites[j].lat) ) * Math.cos( toRadians( pgeSites[i].lat ) ) * Math.cos( toRadians( pgeSites[i].lng ) - toRadians(ffvlSites[j].lng) ) + Math.sin( toRadians(ffvlSites[j].lat) ) * Math.sin( toRadians( pgeSites[i].lat ) ) ) );			
				if (pgeSites[i].ffvl_suid == ffvlSites[j].id ) {
					match = j;
					//break;
				}
			}
			row += '<td>'+ffvlSites[match].id+'</td><td a href="" target="_blank">'+ffvlSites[match].name+'</td><td>'+ffvlSites[match].lat+'</td><td>'+ffvlSites[match].lng+'</td><td><a href="http://paragliding.earth/?latlng='+ffvlSites[match].lat+','+ffvlSites[match].lng+'" target="_blank">go see</a></td><td id="pair'+pgeSites[i].id+'"><a href="#" onclick="updateGPS('+pgeSites[i].id+','+ffvlSites[match].lat+','+ffvlSites[match].lng+')">update gps</a></td></tr>';
			$('#theTable > tbody:last-child').append(row);
	    //	if (pgeSites[i].ffvl_suid == ffvlSites[match].id) {
		//		$('#pair'+pgeSites[i].id).html("update gps ?");
		//		$('#row'+pgeSites[i].id).css("background-color","green");
	    //	}
			match = 0;
		}
	});
	
	console.log(p);
	
function updateGPS(pge, lat, lng){
		// alert('pge:'+pge+', ffvl:'+ffvl);
		$.getJSON( "ajax_update_gps.php", { lat: lat, lng: lng, pge: pge }, function (data) {
			if(data == 'ok!') {
				alert('#row'+pge);
				$('#row'+pge).hide();
				// $('#pair'+pge).html("paired");
			}
			else $('#row'+pge).css("background-color","red");
		});
}

	
// Converts from degrees to radians.
function toRadians(degrees) {
  return degrees * Math.PI / 180;
};

</script>


</body>
</html>
