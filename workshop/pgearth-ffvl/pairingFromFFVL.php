<?php
include "../connexion.php";
//$q = "SELECT id, lat, lng, ffvl_suid FROM `site` WHERE `iso` LIKE 'fr' ";
//$r = mysqli_query($bdd, $q);
?>
<html>
<body>
	<style>
		tr {
			border: 1px solid black;
		}
	</style>
<div id="loading"><img src="https://venturebeat.com/wp-content/uploads/2014/10/loading_desktop_by_brianmccumber-d41z4h6.gif?fit=578%2C325&strip=all"/></div>
<table id="theTable">
	<thead>
		<tr>
			<td>ffvl SUID</td>
			<td>ffvl numero déco</td>
			<td>ffvl nom</td>
			<td>pge id</td>
			<td>pge nom</td>
			<td>distance</td>
			<td>pair sites</td>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="recap"></div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>

<script>
	
	jQuery.ajaxSetup({async:false});
	var ffvl_data = [];
	var paired_sites=0;
	var unpaired_sites=0;
	var total_sites=0;
	$("#theTable").hide();	
	var getData =	$.getJSON("http://data.ffvl.fr/json/sites.json", function( result ) {
						ffvl_data = result;
					});
	
	getData.done( function () {
//		console.log('tutu');
		$("#loading").hide();
		$("#theTable").show();	
		var distance = 100000;
		
		for (var i = 0; i < ffvl_data.length ; i++) {
			
			var num = ffvl_data[i].numero;
			//alert(num);
			
			if ( ffvl_data[i].site_type == "vol" && num.substr(2,1)=="D" && num.substr(6,1)=="A") {     //          we have a main takeoff
				
				var row = '<tr id="row'+ffvl_data[i].suid+'" ><td>'+ffvl_data[i].suid+'</td><td>'+ffvl_data[i].numero+'</td><td>'+ffvl_data[i].nom+'</td>';
				
				total_sites++;
				
				$.getJSON( "search_pge_around.php", { lat: ffvl_data[i].lat, lng: ffvl_data[i].lon }, function( data ) {
					//row += '</tr>';
					row += '<td>'+data.id+'</td><td>'+data.name+'</td><td>'+data.distance+'</td><td id="pair'+ffvl_data[i].suid+'"><span onclick="pair('+data.id+','+ffvl_data[i].suid+')">pair sites</span></td></tr>';
					distance = data.distance;
					suid = data.ffvl_suid;
					pge_id = data.id;
				}).done(function() {

					$('#theTable > tbody:last-child').append(row);
					if (suid == ffvl_data[i].suid ) {
						 $('#row'+ffvl_data[i].suid).css("background-color","green");
						 $('#pair'+ffvl_data[i].suid).html("paired");
						 paired_sites++;
					}
					else if (distance < 0.1)  { unpaired_sites++; pair(pge_id,ffvl_data[i].suid); }
					else if (distance < 0.25) { unpaired_sites++; $('#row'+ffvl_data[i].suid).css("background-color","lightgreen"); }
					else if (distance < 0.5)  { unpaired_sites++; $('#row'+ffvl_data[i].suid).css("background-color","lightblue");  }
					else                      { unpaired_sites++; }
					
					$("#recap").html(paired_sites+" paired sites <br />"+unpaired_sites+" unpaired sites<br />"+total_sites+" total sites");
				});
				
				//	k++;
			}
		}
	//	console.log(k);
	
	});


function pair(pge, ffvl){
		// alert('pge:'+pge+', ffvl:'+ffvl);
		$.getJSON( "pair_sites.php", { ffvl: ffvl, pge:pge }, function (data) {
			if(data.result == 'done') {
				//$('#row'+ffvl).hide();
				paired_sites++;
				unpaired_sites--;
				$('#row'+ffvl).css("background-color","green");
				$('#pair'+ffvl).html("paired");
			}

			else $('#row'+ffvl).css("background-color","red");
		});
}

</script>

</body>
</html>
