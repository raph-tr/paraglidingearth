<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Help us translate in your language</title>
</head>
<body>
	<form name="selectLanguage">
	<?php
		include("languageCodes.php");
		echo "	<select id='languageSelect'>";
		foreach ($language_codes as $code => $language) {
			if ($code <> "en" ) {
				echo "		<option value='".$code."'>".$code." -> ".$language."</option>";
			}
		}
		echo "	</select>";
	?>
	</form>

	<hr />

	<form name="translation">
		
	</form>


	<div id="translateForm"></div>
	<input type='button' id='saveTranslations' value='Save translations' />
	<div id="selectedLanguage"></div>

	<script src="../jquery.js"></script>
	<script>

		var refTerms = {};
		var transTerms = {};
		var langCode="";

		$(document).ready(function () {

			$.getJSON("../lang/en.json", function(data) {

				refTerms = data;
				$.each( refTerms, function( name, value ) {
					$("#translateForm").append( "\"" + value + "\" : <input id='en_" + name + "' /><br/>" );
				});

			});


			$("#languageSelect").on('change', function(){

				langCode = $("#languageSelect").find(":selected").attr("value");


				$.getJSON("../lang/"+langCode+".json", function( data ) {

					transTerms = data;
					$.each( refTerms, function( name, value ) {

						if (name in transTerms) {
							$("#en_"+name+"").val(transTerms[name] );
						} else {
							$("#en_"+name+"").val("");
						}
						
					});
				});

				//$("#saveTranslations").css("display:block");

			});

			$("#saveTranslations").on("click", function () {
				$.each(refTerms, function( name, value ) {
					transTerms[name] = $("#en_"+name+"").val();
				});
				console.log(transTerms);
				console.log(langCode);
				$.post('saveTranslations.php',{ data : JSON.stringify(transTerms), code : langCode } );
			});

		});
	</script>
</body>
</html>