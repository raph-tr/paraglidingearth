var langCode = '';
var defaultLangCode = 'en';
var byDefault = {};

translate(defaultLangCode);

$.getJSON("lang/"+defaultLangCode+".json", function(data) {
	byDefault = data;
});


function translate(code) {

	$.getJSON("lang/"+code+".json", function(data) {

		$("ml").each( function() {
			if ( strTr = data[ $(this).attr("mlKey") ] ) {
				$(this).html (strTr);
			} else {
				$(this).html ( byDefault[ $(this).attr("mlKey") ] );
			}
		});
	});

}