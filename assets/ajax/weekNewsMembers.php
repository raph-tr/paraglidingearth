<?php
include "../../connexion.php";

$query = "	SELECT username, register_date
			FROM as_users
			WHERE DATE(register_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
			ORDER BY register_date DESC ";

$result = mysqli_query($bdd, $query);
$iso = "";

echo "<div>";

$htmlNo = "no new members...";
$html = "<p>This week new members</p><ul>";

while ($val = mysqli_fetch_array($result)){
    $htmlNo = "";
	$html .= " <li><a href='#' class='openAnotherModal' modalToOpen='memberModal' member='".$val['username']."'>".$val['username']."</a>, registered on ".$val['register_date']."</li>";

	}
	
echo $htmlNo.$html."</ul>";
echo "</div>";

?>
