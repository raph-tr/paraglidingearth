<?php
header('Content-Type: application/json');

include "../../connexionS.php";

$south = $_GET['south'];
$north = $_GET['north'];
$east  = $_GET['east'];
$west  = $_GET['west'];
$limit = $_GET['limit'];
$country = $_GET['country'];
$kinds   = $_GET['kinds'];
$winds   = $_GET['winds'];
$flyable = $_GET['flyable'];



$adminArea_id = $_GET['adminArea_id'];



$ignoreSites = $_GET['ignore'];

if ($ignoreSites) $queriedSites = $ignoreSites;
else $queriedSites = [];

$ignoreSitesClause = "";
if(count($ignoreSites) > 0){
  foreach($ignoreSites as $ignoreSite){
	  $ignoreSitesClause .= " site.id != ".$ignoreSite." and ";
	  }
}

$kindClause="";
foreach($kinds as $kind){
   $kindClause .= " ".$kind." = 1 and "; 
}
//  echo $kindClause;

$windClause = "(";
foreach($winds as $wind){
   if ($windClause <> "(" ) $windClause .= " or ";
   $windClause .= " ".$wind." > 0 "; 
}

if ($windClause == "(") $windClause = "";
else $windClause .= " ) and ";



$countryWhereClause = "";
if ($country<>"all" and $country<>"") {
	  $countryWhereClause  = " site.iso = '".$country."' and ";
}

$adminAreaWhereClause = "";
if ($limit=='adminArea' && $adminArea_id<>"" ) {
	  $adminAreaWhereClause  = " site.region = ".$adminArea_id." and ";
}

$hasGPSClause = " and (site.lat !=0 and site.lng !=0) ";
$idWhereClause = " and 1=1 ";

$maxNumberOfSitesToQuery = 200 - count($ignoreSites) ;




$boundsClause = "site.lng < $east and site.lng > $west and site.lat > $south and site.lat < $north";

$query = "SELECT site.lng, site.lat, site.id, site.name, site.iso, site.closed, site.ranking, site.number_votes, site.takeoff_altitude, site.landing_altitude
           FROM site 
             WHERE  $ignoreSitesClause $kindClause $windClause $countryWhereClause $adminAreaWhereClause $boundsClause $idWhereClause $hasGPSClause 
                ORDER BY RAND()
                  LIMIT ".  $maxNumberOfSitesToQuery.";";


$queryAll = "SELECT site.id 
              FROM site 
                WHERE $boundsClause $hasGPSClause";


if ( $_GET['debug'] == 1 ) echo $query;


$res = mysqli_query($bdd, $query);
$queried = mysqli_num_rows($res) ;
$numDisplayed = $queried + count($ignoreSites);

if ($numDisplayed >= 200 or $kindClause<>"" or $windClause<>"" ) {
  $resAll = mysqli_query($bdd, $queryAll);
  $numAll = mysqli_num_rows($resAll);
} else {
  $numAll = $numDisplayed;
}  


$out='{  "total": '. $numAll .',
  
"type": "FeatureCollection",                                                             
"features": [';


while ($result = mysqli_fetch_array($res)){
    array_push($queriedSites, $result['id']);
    if ($result['takeoff_altitude'] == NULL OR $result['landing_altitude'] == NULL ) {
		$deniv = -1;
	} else {
		$deniv = $result['takeoff_altitude'] - $result['landing_altitude'];
	}
    $out .='
        {"type": "Feature", "id": '. $result['id'] .', 
        	"properties": {
        		"NAME": '. json_encode($result['name']) .',
        		"country": '.json_encode(strtolower($result['iso'])).',
        		"closed": '.json_encode($result['closed']).',
        		"takeoff_altitude": '.json_encode((int)$result['takeoff_altitude']).',
        		"landing_altitude": '.json_encode((int)$result['landing_altitude']).',
        		"denivelation": '.json_encode($deniv).',
        		"ranking": '.json_encode($result['ranking']).',
        		"number_votes": '.json_encode($result['number_votes']).',
            "filters": 1,
            "opacity": 1
        	},
        	"geometry": {	
        		"type" : "Point",
        		"coordinates" : ['. $result['lng'] .', '. $result['lat'] .']
        	}
        },';

}


if ( $kindClause != "" or $windClause != "") {

    /***********  we query sites that don't have relevant information for filters ***************/
    if ($numDisplayed < $maxNumberOfSitesToQuery) {

        $ignoreSitesClause = " site.id NOT IN (".implode(",", $queriedSites).") AND ";

        if ($kindClause <> "") {
          $kindClause = " `hike`=0 and `soaring`=0 and `thermal`=0 and `xc`=0 and `flatland`=0 and `winch`=0 and `hanggliding`= 0 and ";
        }
        if ($windClause <> ""){
          $windClause = " `N`=0 and `NE`=0 and `E`=0 and `SE`=0 and `S`=0 and `SW`=0 and `W`=0 and `NW`=0 and ";
        }

        $limitQ =  $maxNumberOfSitesToQuery-$numDisplayed;

        $query = "SELECT site.lng, site.lat, site.id, site.name, site.iso, site.closed, site.ranking, site.number_votes, site.takeoff_altitude, site.landing_altitude
                   FROM site 
                     WHERE  $ignoreSitesClause $kindClause $windClause $countryWhereClause $boundsClause $idWhereClause $hasGPSClause 
                        ORDER BY RAND()
                          LIMIT ". $limitQ .";";

       // $out .= '{ "query2" : "'.json_encode($query).'"} ,';                  
        $res = mysqli_query($bdd, $query);

        while ($result = mysqli_fetch_array($res)){

            array_push($queriedSites, $result['id']);
            $numDisplayed++;
            $out .='
                {"type": "Feature", "id": '. $result['id'] .', 
                  "properties": {
                    "NAME": '. json_encode($result['name']) .',
                    "country": '.json_encode(strtolower($result['iso'])).',
                    "closed": '.json_encode($result['closed']).',
                    "takeoff_altitude": '.json_encode($result['takeoff_altitude']).',
                    "landing_altitude": '.json_encode($result['landing_altitude']).',
                    "ranking": '.json_encode($result['ranking']).',
                    "number_votes": '.json_encode($result['number_votes']).',
                    "filters": -1,
                    "opacity": 0.4
                  },
                  "geometry": { 
                    "type" : "Point",
                    "coordinates" : ['. $result['lng'] .', '. $result['lat'] .']
                  }
                },';

        }
    }

    /***********  we query sites that don't sastisfy the filters ***************/
    if ($numDisplayed < $maxNumberOfSitesToQuery) {

        $ignoreSitesClause = " site.id NOT IN (".implode("," , $queriedSites).") AND ";

        $kindClause = "";
        $windClause = "";

        $limitQ =  $maxNumberOfSitesToQuery-$numDisplayed;

        $query = "SELECT site.lng, site.lat, site.id, site.name, site.iso, site.closed, site.ranking, site.number_votes, site.takeoff_altitude, site.landing_altitude
                   FROM site 
                     WHERE  $ignoreSitesClause $kindClause $windClause $countryWhereClause $boundsClause $idWhereClause $hasGPSClause 
                        ORDER BY RAND()
                          LIMIT ". $limitQ .";";

        // $out.='{"query":"'.json_encode($query).'"},';                 

        $res = mysqli_query($bdd, $query);

        while ($result = mysqli_fetch_array($res)){
            array_push($queriedSites, $result['id']);
            $numDisplayed++;
            $out .='
                {"type": "Feature", "id": '. $result['id'] .', 
                  "properties": {
                    "NAME": '. json_encode($result['name']) .',
                    "country": '.json_encode(strtolower($result['iso'])).',
                    "closed": '.json_encode($result['closed']).',
                    "takeoff_altitude": '.json_encode($result['takeoff_altitude']).',
                    "landing_altitude": '.json_encode($result['landing_altitude']).',
                    "ranking": '.json_encode($result['ranking']).',
                    "number_votes": '.json_encode($result['number_votes']).',
                    "filters": 0,
                    "opacity": 0.4

                  },
                  "geometry": { 
                    "type" : "Point",
                    "coordinates" : ['. $result['lng'] .', '. $result['lat'] .']
                  }
                },';

        }
    }


}





if ($numDisplayed > 0) $out = substr($out, 0, -1);

$out .= '
] ,
  "displayed" : '.$numDisplayed.'
}';


echo $out;
?>
