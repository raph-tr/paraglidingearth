<?php

function hours( $seconds ) {
  
  $minutes = floor($seconds / 60) ;
  $sec = $seconds - 60*$minutes ;  
  
  $h = floor($minutes / 60) ;
  $min = $minutes - 60*$h ;

  
  return $h.":".$min.":".$sec;
}

function fetchURL( $url, $timeout=5) {
   $url_parsed = parse_url($url);
   $host = $url_parsed["host"];
   $port = $url_parsed["port"];
   if ($port==0)
       $port = 80;
   $path = $url_parsed["path"];
   if ($url_parsed["query"] != "")
       $path .= "?".$url_parsed["query"];

   $out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";

   $fp = @fsockopen($host, $port, $errno, $errstr, $timeout);
   if (!$fp) { return 0; }

   stream_set_timeout($fp,$timeout);

   if (fwrite($fp, $out)) {
       $body = false;
       while (!feof($fp)) {
           if ( ! $s = fgets($fp, 1024) ) {
                //echo "#";
                break;
            }
           if ( $body )
               $in .= $s;
           if ( $s == "\r\n" )
               $body = true;
       }
   }  else {
    //echo "$";
   }
   fclose($fp); 
   return $in;
}


$lat = $_REQUEST['lat'];
$lng = $_REQUEST['lng'];

$leonardo_description = "<b><a href=\'http://www.paraglidingforum.com/modules.php?name=leonardo&op=list_flights\' target=\'_blank\'>Leonardo</a> offers you to upload your flights worldwide<br />and participe in a friendly XC chalenge,<br />with automatic flying distances and points awards calculations.</b>";

$pgforum = fetchURL('http://www.paraglidingforum.com/modules/leonardo/EXT_flight.php?op=find_flights&lat='.$lat.'&lon='.$lng);

$pos = strpos ($pgforum,"paraglidingforum.com");

if ($pos === false){
	echo "false";
}


else {

	$xml = simplexml_load_string ($pgforum);

	$leonardo_wpt = $xml->waypoints[0];

	$leonardo_wpt_name = $xml->waypoints->waypoint[0]->intName;

	$lat2 = floatval ($xml->waypoints->waypoint[0]->lat);
	$lng2 = floatval ($xml->waypoints->waypoint[0]->lon);
	$leonardo_wpt_id = $xml->waypoints->waypoint[0]->id;


	$lat2rad = 3.14159265 * $lat2 / 180;
	$lng2rad = 3.14159265 * $lng2 / 180;
	$latrad  = 3.14159265 * $lat  / 180;
	$lngrad  = 3.14159265 * $lng  / 180;


	$distance = round(6366 * acos(cos($latrad) * cos($lat2rad) * cos($lng2rad-$lngrad) + sin($latrad) * sin($lat2rad)),2);
	//echo $lat2.", ".$lng2.", ".$distance;
	
	if ($distance<25 and isset($xml->flights->flight[0]->displayLink)){
		?>


		<table class="table table-striped table-bordered table-condensed">
		  <tr>
			<td colspan="7">
			  <a href="#" onclick="$('.leonardo-details').toggle(600);"><img src="assets/img/leonardo.gif" height="38px"> <b>Flights on Leonardo</b> <i class="fa fa-eye-slash"></i></a>
			</td>
		  </tr>

		  <tr class="leonardo-details">
			<td colspan="7">
			  <i>takeoff: <?php echo $leonardo_wpt_name; ?> (<?php echo $lat2; ?>; <?php echo $lng2; ?>), <?php echo $distance; ?> km from this takeoff</i>
		<?php
		$compare = ''; 
		$count = 0;
		
		foreach ($xml->flights->flight as $flight) {
		  if ($count < 10) {
			$string = $flight->displayLink;
			$matches = array();
			if (preg_match('#(\d+)$#', $string, $matches)) {
			  $id = $matches[1];
			}
			$compare .= $id.','; $count++;
		  }
		}
		?>
		<br /><b><a href="http://www.paraglidingforum.com/leonardo/compare/<?php echo $compare; ?>" target="_blank">Top ten Leonardo flights at a glance. <i class="fa fa-external-link"></i></a></b>
			  
			</td>
		  </tr>

		  <tr class="small leonardo-details">
			<td align="center"><b>Pilot</b>
			</td>
			<td align="center"><b>OLC type</b>
			</td>
			<td align="center"><b>OLC distance (km)</b>
			</td>
			<td align="center"><b>Open distance (km)</b>
			</td>
			<td align="center"><b>Flight duration</b>
			</td>
				<td align="center"><b>Date</b>
			</td>
			<td align="center"><b>View flight</b></a>
			</td>
		  </tr>
		<?php
		$pair=0;
		$count=0;
		foreach ($xml->flights->flight as $flight) {
		   
		   $count++;
		   
		   if ($count==13) {
			 ?>
			  <tr class="small leonardo-details">
				<td align="center"><a href="http://www.paraglidingforum.com/modules.php?name=leonardo&op=list_flights&takeoffID=<?=$leonardo_wpt_id?>&year=0&month=0&pilotID=0&country=0&cat=0" target="_blank">view all...</a>
				</td>
				<td align="center" valign="middle">
				</td>
				<td align="center">
				</td>
				<td align="center">
				</td>
				<td align="center">
				</td>
				<td align="center">
				</td>
				<td align="center">
				</td>
			  </tr>
			 <?php
			 break;
		   }
   
		   switch ($flight->OLCtype){
			  case "FREE_FLIGHT":
				 $leonardo_icon = "icon_turnpoints.gif";
				 $leonardo_icon_alt = "free flight";
			  break;
			  case "FREE_TRIANGLE":
				 $leonardo_icon = "icon_triangle_free.gif";
				 $leonardo_icon_alt = "free triangle";
			  break;
			  case "FAI_TRIANGLE":
				 $leonardo_icon = "icon_triangle_fai.gif";
				 $leonardo_icon_alt = "fai triangle";
			  break;
			}
		?>
		  <tr class="small leonardo-details">
			<td align="center"><?php echo $flight->pilot ?>
			</td>
			<td align="center" valign="middle"><img src="assets/img/<?php echo $flight->OLCtype; ?>.gif" title="<?php echo strtolower($flight->OLCtype); ?>" />
			</td>
			<td align="center"><?php echo ($flight->OLCkm / 1000) ; ?>
			</td>
			<td align="center"><?php echo ($flight->openDistance / 1000) ;?>
			</td>
			<td align="center"><?php echo (hours(intval($flight->duration))) ; ?>
			</td>
			<td align="center"><?php echo ($flight->date) ; ?>
			</td>
			<td align="center"><a href="<?php echo $flight->displayLink ; ?>" target="_blank">View <i class="fa fa-external-link"></i></a>
			</td>
		  </tr>
		<?php

			} ?>
		</table>

		<?php
 
	} else { 
?>
		<table class='table table-striped table-bordered table-condensed'>
		  <tr>
			<td >
				  <img src="assets/img/leonardo.gif" height="38px"> <b>&nbsp;Flights on Leonardo</b>
				</td>
			  </tr>
			  <tr>
				<td>
				No flight available in a 25 km range.<br />
			  Closest flight in Leonardo is from <?php echo $xml->waypoints->waypoint[0]->intName." : ".$distance;?> km away from this site
			</td>
		  <tr>
		</table>
<?php
	}
}

?>
