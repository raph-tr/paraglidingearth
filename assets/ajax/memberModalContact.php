<?php
include '../../member/ASEngine/AS.php';

$db = app('db');
$result = $db->select(
    "SELECT * FROM `as_user_details` LEFT JOIN as_users ON as_user_details.user_id = as_users.user_id WHERE `username` = :username",
    array ("username" => $_POST['userName'])
);
$userDetails = $result[0];

if ( $userDetails['contact_form']==1 ) {
?>

<fieldset>
	<legend>Contact Form</legend>
	<!--
	<p class="note">
	  This is an example PHP form that processes user information, checks for errors, and validates the captcha code.<br />
	  This example form also demonstrates how to submit a form to itself to display error messages.
	</p>
	-->
	<div id="member_success_message" class="alert-info" style="display: none">
		<p>	Your message has been sent <span id="destUserName"></span>!
		</p>
	</div>

	<form method="post" action="" id="member_contact_form" onsubmit="return processMemberContactForm()">
	  <input type="hidden" name="do" value="contact" />
	  <input type="hidden" name="dest" value="<?php echo $_POST['userName']; ?>" />
	  <div class="well well-sm">				  
		<div class="row">
		  <div class="col-md-4">
			<div class="form-group">
			  <label for="ct_name">Name:</label>
			  <input type="text" name="ct_name"   class="form-control" value="" />
			</div>
			<div class="form-group">
			  <label for="ct_email">Email:</label>
			  <input type="text" name="ct_email"  class="form-control" value="" />
			</div>
		  </div>
		  <div class="col-md-8">
			<label for="ct_message">Message:</label>
			<textarea name="ct_message" rows="5"  class="form-control"></textarea>
		 </div>
		</div>
		<div class="row">
		  <div class="col-md-12">
			<label for="captcha_code">Be a human, make the maths:</label><br />
			<?php require_once '../captcha/securimage.php'; echo Securimage::getCaptchaHtml(array('input_name' => 'ct_captcha')); ?>
		 </div>
		</div>
		<div class="row">
		  <div class="col-md-12">
			<input type="submit" value="Send message to <?php echo $_POST['userName']; ?>" class="btn btn-primary pull-right" />
		  </div>
		</div>
	  </div>
	</form>
</fieldset>

<hr />

<!-- Make sure that this script file is included on the page after you include jQuery -->
<script src="../../member/ASLibrary/js/js-bootstrap.php"></script>
<script>
	/*
//	$('#aboutModal').on('shown.bs.modal', function(){
		console.log('hop');
		$( "#contact" ).load( "assets/captcha/example_form.ajax.php", function() {
			console.log( "Form loaded" );
//		});
	 //   $.noConflict();
		function reloadCaptcha()
		{
			$('#siimage').prop('src', './assets/captcha/securimage_show.php?sid=' + Math.random());
		}
	});
/**/
	function processMemberContactForm()
	{
		$.ajax({
			url: 'assets/captcha/mailerMember.php',
			type: 'POST',
			data: $('#member_contact_form').serialize(),
			dataType: 'json',
		}).done(function(data) {
			if (data.error === 0) {
				$('#member_success_message').show();
				$('#destUserName').html('to '+data.dest);
				$('#member_contact_form')[0].reset();
				reloadCaptcha();
				setTimeout("$('#member_success_message').fadeOut()", 12000);
			} else {
				alert("There was an error with your submission.\n\n" + data.message);
				if (data.message.indexOf('Incorrect security code') >= 0) {
					$('#captcha_code').val('');
				}
			}
		});
		return false;
	}
</script>
<?php 
	} else {
?>

<p>
	Sorry, it looks like this user did not wish to be contacted via this website... 
</p>

<?php		
		}
?>
