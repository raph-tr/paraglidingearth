	<div class="row">
		<div class="col-md-12">
			<div class="well well-sm">
				<div id="returnMessage"> </div>
                <form class="form-horizontal" id="formDelete">
                    <fieldset>
						
						<legend>
							So you think the <?php echo $_POST['itemType'];?> "<?php echo $_POST['itemName'];?>" should be erased ?
						</legend>
							<input id="itemId"   name="itemId"   type="hidden" value="<?php echo $_POST['itemId'];?>"   >
							<input id="itemName" name="itemName" type="hidden" value="<?php echo $_POST['itemName'];?>" >
							<input id="itemType" name="itemType" type="hidden" value="<?php echo $_POST['itemType'];?>" >
 
                        <div class="form-group">
                            <span class="col-md-12"><i class="fa fa-user bigicon"></i> * Your name</span>
                            <div class="col-md-12">
                                <input id="nameRequestDelete" name="name" type="text" placeholder="* Your name" class="form-control">
                            </div>
                        </div>
 
                        <div class="form-group">
                            <span class="col-md-12"><i class="fa fa-envelope-o bigicon"></i> * Your email</span>
                            <div class="col-md-12">
                                <input id="emailRequestDelete" name="email" type="text" placeholder="* Your email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-12"><i class="fa fa-pencil-square-o bigicon"></i> * Reason(s) to delete the <?php echo $_POST['itemType'];?></span>
                            <div class="col-md-12">
                                <textarea class="form-control" id="messageRequestDelete" name="message" placeholder="* Please let us know why this <?php echo $_POST['itemType'];?> should be erased." rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <input type="button" id="submitDelete" class="btn btn-primary btn-lg" value="Send"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

<script>
	
$(document).ready(function() {

	$("#submitDelete").click(function() { 
		var name = $("#nameRequestDelete").val();
		var email = $("#emailRequestDelete").val();
		var message = $("#messageRequestDelete").val();
		var itemId = $("#itemId").val();
		var itemName = $("#itemName").val();
		var itemType = $("#itemType").val();
		
		$("#returnMessage").html(""); // To empty previous error/success message.
		
		// Checking for blank fields.
		if (name == '' || email == '' || message == '') {
			alert("Please Fill Required Fields");
		} else {
			// Returns successful data submission message when the entered information is stored in database.
			$.post("assets/ajax/reportItemSaveAndMail.php", $('form#formDelete').serialize(), function(data) {
						$("#returnMessage").append(data); // Append returned message to message paragraph.
						console.log("data: "+data);
									 
						if (data == "Request was sent to site moderators, thanks for your participation.") {
							$("#formDelete")[0].reset(); // To reset form fields on success.
				            $("#formDelete").hide();
				         //   $("#returnMessage").html("Request was sent to site moderators, thanks for your participation.");
						}
			});
		}
	});
});
</script>

