<?php

include dirname(__FILE__) . '/../../member/ASEngine/AS.php';

if (! app('login')->isLoggedIn()) {
	$member_edit = 0;
} else {
	$member_edit = 1;
}

header('Content-Type: application/json; charset=utf-8');
//session_start();

$site_id  = $_GET['id'];
include "../../connexionS.php";

//echo "logged : ".$_SESSION['user_login_status'];
if (isset($_SESSION['userFullName'])) $member_edit = 1;


$query = "SELECT *, username FROM site left join as_users on author=user_id WHERE site.id=".$site_id .";"; // used to get some info 
$result = mysqli_query($bdd, $query);


/****** there is a site to show *****************/
if ( mysqli_num_rows($result) > 0) {

		$val = mysqli_fetch_array($result);

		$query_country = "SELECT name FROM country WHERE country.id=".$val['country'].";";
		$result_country = mysqli_query($bdd, $query_country);
		$val_country = mysqli_fetch_array($result_country);


		$out = '{  
		"result":1,
		';

		$out .= '	"body": { ';


		$queryBody = "SELECT * FROM site WHERE site.id=".$site_id .";";
		$resultBody = mysqli_query($bdd, $queryBody);



		while($row = mysqli_fetch_assoc($resultBody)) {
			foreach($row as $key => $value) {
			$out .= '
				"'.$key.'" : '.json_encode($value).',';
			}
		}
		$out  = substr($out, 0, -1);
		$out .='
			},
		';


		$out.= '	"flag": '.json_encode('<img src="assets/img/flag/_tn_'.$val['iso'].'.png" title="'.$val_country['country.name'].'" />').',
		';

		$star = $val['ranking'];
		$votes = $val['nombre_votes'];
		//include "../images/star_return_string.php";

		//  $out .= '	"rating": '.json_encode($string).',';

		$kindofflights=$val['condition'];
		$winch=$val['winch'];
		$hanggliding=$val['hanggliding'];
		$imagesize=40;     
		// include "../images/kindofflightssmall_returnphpstring.php";

		//  $out .= '	"kinds_of_flight_img": '.json_encode($string).',';

		  $rose = '<img src="assets/img/windrose/26/'.$val['site.id'].'.png" />';

		  $out .= '	"orientation_img": '.json_encode($rose).',
		';

		  $title = $rose . ' ' . $val['name'] . ' ' . $string;
		 
		$rPics = mysqli_query($bdd, "select id, author, name, comment from picture where site = $site_id ");
		$out .= '	"pictures" : [   '	;
		while ($vPics = mysqli_fetch_array($rPics)){
			$out .= '
					{"picture" : "'.$vPics['name'].'",
					 "comment" : '.json_encode($vPics['comment']).',
					 "author" : '.json_encode($vPics['author']).',
					 "id" :  '.json_encode($vPics['id']).'
					 },';
		}
		$out = substr($out, 0, -1);
		$out .='],';
		  
		$rPros = mysqli_query($bdd, "select pro_site.pro, pro.name, pro.iso from pro_site left join pro on pro_site.pro = pro.id where pro_site.site = $site_id ");
		$out .= '
			"pros" : [   '	;
		while ($vPros = mysqli_fetch_array($rPros)){
			$out .= '
				{"pro" : '.json_encode($vPros['name']).',
				"id": '.$vPros['pro'].',
				"iso": '.json_encode(strtolower($vPros['iso'])).'
				},';
		}
		$out = substr($out, 0, -1);
		$out .='
			],';

		$rClubs = mysqli_query($bdd, "select club_site.club, club.name, club.iso from club_site left join club on club_site.club = club.id where club_site.site = $site_id ");
		$out .= '
			"clubs" : [   '	;
		while ($vClubs = mysqli_fetch_array($rClubs)){
			$out .= '
				{"club" : '.json_encode($vClubs['name']).',
				"id": '.$vClubs['club'].',
				"iso": '.json_encode(strtolower($vClubs['iso'])).'
				},';
		}
		$out = substr($out, 0, -1);
		$out .='
			],';


		 $out .= '
			"history" : [   '	;

		/* *******   first user in history is the site author ****
			$out .= '
				{ "userName" : '.json_encode($val['username']).',
				"userId": '.json_encode($val['author']).',
				"userDate": '.json_encode(strtolower($val['date_site'])).'
				},';
		/** **/
		/* ********* users modifications from in the users_sites table   ******/
		$qHistory= "
		SELECT username AS userName, user_id AS userId, sites_users.date AS userDate, GROUP_CONCAT(DISTINCT  sites_users.modification) AS userModification
		 FROM sites_users
		 LEFT JOIN as_users ON user_id = sites_users.user
		 WHERE sites_users.item_id = ".$site_id."
		  AND  sites_users.item_type LIKE 'site'
		 GROUP BY userId, userDate
		 ORDER BY userDate DESC";
		 
		  $rHistory = mysqli_query($bdd, $qHistory);

		  while ($vHistory = mysqli_fetch_array($rHistory)){
			$out .= '
				{ "userName" : '.json_encode($vHistory['userName']).',
				"userId": '.json_encode($vHistory['userId']).',
				"userDate": '.json_encode(strtolower($vHistory['userDate'])).',
				"userModification": '.json_encode(strtolower($vHistory['userModification'])).'
				},';
		 }
		$out = substr($out, 0, -1);
		$out .='
			],';



		$out .= '
			"iso": '.json_encode($val['iso']).',
			"country_name" : '.json_encode($val_country['name']).',
			"lat": '.$val['lat'].',
			"lng": '.$val['lng'].',
			"member_edit": '.json_encode($member_edit).'
		}';


		echo $out;
} else {
	
	echo '{ "result":0, "body": { "name" : "Site not found... :(" }, "lat": 0, "lng": 0, "member_edit":0 }';
	
}

?>
