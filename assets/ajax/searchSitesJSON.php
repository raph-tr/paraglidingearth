<?php
//  header("Content-type: text"); 
header('Content-Type: application/json');

include "../../connexion.php";

$query = "select site.id, site.lng, site.lat, site.name, iso from site where site.lng != 0 and site.lat != 0 and site.name like '%".$_GET['name']."%' limit 5";
$res = mysqli_query($bdd, $query);
$queried = mysqli_num_rows($res) ;

//  echo $query."<br />";

$out='{
"features": [';
while ($result = mysqli_fetch_array($res)){
    $out .='{"id": '. $result['id'] .',
          "name": '. json_encode($result['name']) .',
          "countryCode": '.json_encode(strtolower($result['iso'])).',
          "lng": '. $result['lng'] .',
          "lat": '. $result['lat'] .'
      },';
}
if ($queried > 0) $out = substr($out, 0, -1);
$out .= '
   ] 
}';

echo $out;
?>
