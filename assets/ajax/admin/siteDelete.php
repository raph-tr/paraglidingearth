<?php
include "_isAdmin.php";

$siteId = $_GET['site'];

$qSite = "select name, lat, lng, author, id from site where id =".$siteId;
$rSite = mysqli_query($bdd, $qSite);
$vSite = mysqli_fetch_array($rSite);

$qReported = "select reason, requested_by_name from reported_items where type LIKE 'site' and item_id =".$siteId;
$rReported = mysqli_query($bdd, $qReported);
$vReported = mysqli_fetch_array($rReported);

$qDeleted = "INSERT INTO `deleted_item`
				(`id`, `type`, `name`, `reason`, `delete_date`, `request_author`, `item_id`, `original_author`, `lat`, `lng`, `imported`)
				VALUES
				(NULL, 'site', '".$vSite['name']."', '".$vReported['reason']."', NOW(), '".$vReported['requested_by_name']."', '".$vSite['id']."', '".$vSite['author']."', '".$vSite['lat']."', '".$vSite['lng']."', '0');";
if ($rDeleted = mysqli_query($bdd, $qDeleted)) echo "Site added in 'deleted-items' table. <hr />";
else echo "problem while adding site to 'deleted' table : ".$qDeleted."<hr />";
?>

######################## DB TABLES ################################# <br />
<?php 
	$tables_ids = ["site"=>"id", "picture"=>"site", "site_extra_items"=>"site", "club_site"=>"site",  "pro_site"=>"site", "sites_users"=>"item_type` LIKE 'site' AND  `item_id"];
//	var_dump($tables_ids);

	foreach ($tables_ids as $table => $field ) {

		$q = "DELETE FROM `".$table."` WHERE `".$field."`=".$siteId.";";
		echo "table '".$table."' : ";
		if ( $r = mysqli_query($bdd, $q) ) echo "done !  ".mysqli_affected_rows($bdd)." rows affected..";
		else echo "fail : ".$q;
		echo "<br />";
	}

	$q = "update reported_items SET done = 1 where `type` LIKE 'site' AND  `item_id` = ".$siteId;
	if ( $r = mysqli_query($bdd, $q) ) echo "table 'reported_item' : done !  ".mysqli_affected_rows($bdd)." rows affected..";
	else echo "fail : ".$q;
	echo "<br />";


?>

<hr />


#######################  Files   #################################### <br />
<?php
foreach (glob("../../img/sites-pictures/".$siteId."-[0-9]*.*") as $filename) {
    if ( unlink($filename) )  echo "Image ".$filename." deleted ! <br/>";
    else echo "problem trying to delete image ".$filename.". <br />";
}
foreach (glob("../../img/windrose/*/".$siteId.".*") as $filename) {
    if ( unlink($filename) )  echo "Image ".$filename." deleted ! <br/>";
    else echo "problem trying to delete image ".$filename.". <br />";
}
foreach (glob("../../img/flying/*/".$siteId.".*") as $filename) {
    if ( unlink($filename) )  echo "Image ".$filename." deleted ! <br/>";
    else echo "problem trying to delete image ".$filename.". <br />";
}
foreach (glob("../../img/calendar/*/".$siteId.".*") as $filename) {
    if ( unlink($filename) )  echo "Image ".$filename." deleted ! <br/>";
    else echo "problem trying to delete image ".$filename.". <br />";
}
?>
