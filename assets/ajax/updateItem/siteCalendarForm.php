<?php
require '_checkLoggedIn.php';

include("../../../connexion.php");


if (isset($_POST['id']) or isset($_GET['id'])) {
	if (isset($_POST['id'])) $id = $_POST['id'];
	if (isset($_GET['id'])) $id = $_GET['id'];
}

$query  = "SELECT * FROM site WHERE id=".$id.";";
$result = mysqli_query($bdd, $query);
$val    = mysqli_fetch_array($result);
//$id = $val['id'];
?>

<div id="calendarForm" class="alert-warning">
<table class="tableCalendar"  style="font-size: smaller;">

    <tr>
        <td class ="titreMenu" colspan="14">Flying in <?=$val['name']?> around the year :
        </td>
    </tr>   
    
    <tr>
        <td><img src="assets/img/icons/famfamfam/weather_sun.png" /> Best</td>
            <?php for ($i=1; $i<13; $i++){?>
                <td><input type="radio" id="month<?php echo $i; ?>.3" name="month<?php echo $i; ?>" value="3" <?php if ($val['month_'.$i]==3) echo "checked"; ?>></td>
            <?php }?>
        <td rowspan="4"><img src="assets/img/calendar/calendar_linear_bg.png" width="10" height="100%"></td>
    </tr>

    <tr>
        <td><img src="assets/img/icons/famfamfam/weather_cloudy.png" /> Good</td>
            <?php for ($i=1; $i<13; $i++){?>
                <td><input type="radio" id="month<?php echo $i; ?>.2" name="month<?php echo $i; ?>" value="2" <?php if ($val['month_'.$i]==2) echo "checked"; ?>></td>
            <?php }?>
    </tr>

    <tr>
        <td><img src="assets/img/icons/famfamfam/weather_clouds.png" /> Not so good</td>
            <?php for ($i=1; $i<13; $i++){?>
                <td><input type="radio" id="month<?php echo $i; ?>.1" name="month<?php echo $i; ?>" value="1" <?php if ($val['month_'.$i]==1) echo "checked"; ?>></td>
             <?php }?>
    </tr>

    <tr>
        <td><img src="assets/img/icons/famfamfam/weather_rain.png" /> Bad</td>
            <?php for ($i=1; $i<13; $i++){?>
                <td><input type="radio" id="month<?php echo $i; ?>.0" name="month<?php echo $i; ?>" value="0" <?php if ($val['month_'.$i]==0) echo "checked"; ?>></td>
             <?php }?>
    </tr>

    <tr>
        <td>Can't tell</td>
            <?php for ($i=1; $i<13; $i++){?>
                <td><input type="radio" id="month<?php echo $i; ?>.-1" name="month<?php echo $i; ?>" value="-1"  <?php if (is_null($val['month_'.$i])) echo "checked"; ?>></td>
            <?php }?>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td>Jan</td>
        <td>Feb</td>
        <td>Mar</td>
        <td>Apr</td>
        <td>May</td>
        <td>Jun</td>
        <td>Jul</td>
        <td>Aug</td>
        <td>Sep</td>
        <td>Oct</td>
        <td>Nov</td>
        <td>Dec</td>
        <td></td>
    </tr>
    
    <tr>
        <td colspan="14" align="center">
        <input type="hidden" name="site" value="<?=$_GET['site']?>">
        <input type="hidden" name="submit" value="1">
<!--        <INPUT type="submit" value="Submit">
-->
		<button id="submitCalendar"  class="btn btn-primary btn-sm">submit calendar</button>
		<button id="cancelCalendar"  class="btn btn-secondary btn-sm">cancel</button>

        </td>
    <tr>

</table>

<hr />
</div>
<script>
	
	$("#cancelCalendar").on("click", function(){
			$("#calendarForm").toggle("slow");
		});
	
	$("#submitCalendar").on("click", function(){
		var months = {"month_1":0,"month_2":0,"month_3":0,"month_4":0,"month_5":0,"month_6":0,"month_7":0,"month_8":0,"month_9":0,"month_10":0,"month_11":0,"month_12":0};
//		console.log(document.getElementById("month2.1").checked);
		for (var i=1; i<13; i++) {
			if (document.getElementById("month"+i+".3").checked)       months[i] = 3;
			else if (document.getElementById("month"+i+".2").checked)  months[i] = 2;
			else if (document.getElementById("month"+i+".1").checked)  months[i] = 1;
			else if (document.getElementById("month"+i+".0").checked)  months[i] = 0;
			else months[i] = -1;
		}
		months[0] = <?php echo $id; ?>;
		console.log(months);
		$.post("assets/ajax/updateItem/siteCalendarSave.php", months, function(data){
				$.get( "assets/img/calendar/draw_calendar.php", { "site": <?php echo $id; ?>, "size": 80, "text": "yes" }, function(data){
					d = new Date();
					$("#calendarImg").attr("src", "assets/img/calendar/80/<?php echo $id; ?>.png?t="+d.getTime());
					$("#calendarForm").toggle("slow");
					} );
		});
//		$("#calendarForm").toggle("slow");	
		});
	
</script>
