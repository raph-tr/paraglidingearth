<form id="activitiesForm" action="javascript:void(0);">
	<input type="hidden" id="idForm" name="idForm" value="<?php echo $_POST['id'];?>" />
	 <div class="checkbox">
		<label><input type="checkbox" id="parties" name="activity[]" value="parties" <?php if($_POST['parties']==1) echo "checked";?> /> <img src="assets/img/icons/parties.png" /> parties, bbqs, and also some flying</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" id="relax" name="activity[]" value="relax" <?php if($_POST['relax']==1) echo "checked";?> /> <img src="assets/img/icons/relax.png" /> relax, cool flying with friends, fun and safety first</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" id="performance" name="activity[]" value="performance" <?php if($_POST['performance']==1) echo "checked";?> /> <img src="assets/img/icons/performance.png" /> serious flying, performance, xc, acro, etc.</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" id="active" name="activity[]" value="active" <?php if($_POST['active']==1) echo "checked";?> /> <img src="assets/img/icons/active.png" /> quite active crew, chances are we're in the sky every single flyable day</label>
	</div>
	<div>
		<button id="submitActivitiesForm"  class="btn btn-primary btn-sm">submit</button>
		<button id="cancelActivitiesForm"  class="btn btn-secondary btn-sm">cancel</button>
	</div>
</form>

<script>
	
	$("#cancelActivitiesForm").on("click", function(){
		$("#activitiesForm").toggle("slow");
		$("#activitiesDiv").toggle("slow");
	});
	
	$("#submitActivitiesForm").on("click", function(){
		var listHtml = '';		
		var dataForm = {'id': $('#idForm').val(), 'parties':0, 'performance':0, 'relax':0, 'active':0 };
		if( $('input[id=parties]').is(':checked') ) {
			dataForm['parties'] = 1;
			listHtml += '<li> <img src="assets/img/icons/parties.png" /> parties, bbqs, and also some flying</li>';
		}
		if( $('input[id=relax]').is(':checked') ) {
			dataForm['relax'] = 1;
			listHtml += '<li> <img src="assets/img/icons/relax.png" /> parties, bbqs, and also some flying</li>';
		}
		if( $('input[id=performance]').is(':checked') ) {
			dataForm['performance'] = 1;
			listHtml += '<li> <img src="assets/img/icons/performance.png" /> parties, bbqs, and also some flying</li>';
		}
		if( $('input[id=active]').is(':checked') ) {
			dataForm['active'] = 1;
			listHtml += '<li> <img src="assets/img/icons/active.png" /> parties, bbqs, and also some flying</li>';
		}
		
	//	console.log (dataForm);
			
		$.post("assets/ajax/updateItem/clubActivitiesSave.php", dataForm, function( data ) {
		//	console.log( "Data Loaded: " + data );
		});

		$("#activitiesDiv").html(listHtml);
		$("#activitiesForm").toggle("slow");
		$("#activitiesDiv").toggle("slow");
	});
	
</script>
