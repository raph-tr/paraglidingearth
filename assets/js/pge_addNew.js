var newItemMarker;
var newItemPopup;

function addNew(item) {

    if ( newItemMarker ) {
		map.removeLayer(newItemMarker);
		map.removeLayer(newItemPopup);
	}
    newItemMarker = L.marker([map.getCenter().lat, map.getCenter().lng], {draggable : true} ).addTo(map);
    newItemPopup  = L.popup().setContent('toto');
    
 
	
	var image = "assets/js/localhost/images/marker-icon.png";
	if (item=="club") image = "assets/js/localhost/images/marker-icon-red.png";
	else  if (item=="pro") image = "assets/js/localhost/images/marker-icon-yellow.png";

	var newItemIcon = L.icon({
		iconUrl: image,
		shadowUrl: 'assets/js/localhost/images/marker-shadow.png',
		iconSize:     [24, 40], // size of the icon
		shadowSize:   [40, 40], // size of the shadow
		iconAnchor:   [12, 40], // point of the icon which will correspond to marker's location
		shadowAnchor: [13, 40],  // the same for the shadow
		popupAnchor:  [0, -40] // point from which the popup should open relative to the iconAnchor

		});
	
  	newItemMarker.setIcon(newItemIcon);
  


	/* **************    create MArker popup form string *************/
 	var Item = item.charAt(0).toUpperCase()+item.slice(1);
	var formStr = "<form id='newForm' class='form-inline' action='javascript:submitForm()'>";
	if (item=="site") formStr += " Drop the marker on the site main takeoff place.";
	formStr += " <fieldset class='newItemFieldset'>";
	formStr += "  <div class='form-group'>";
	formStr += "    <label for='name'>"+Item+" name<\/label>";
	formStr += "    <input type='text' name='name' id='name' maxlength='64' required placeholder='name' \/>";
	formStr += "    <br \/>";
	if (item != "site") formStr += "	<label for='city'>City<\/label>";
	if (item != "site") formStr += "    <input type='text' name='city' id='cityNewItem' required placeholder='city' \/>";
	if (item != "site") formStr += "    <br \/>";
	formStr += "	<label for='iso'>Country<\/label>";
	formStr += "    <input type='text' name='iso' id='iso' required placeholder='iso' readonly='readonly'\/>";
	formStr += "    <br \/>";
	if (item == "site") formStr += "	<label for='alt'>Elevation<\/label>";
	if (item == "site") formStr += "    <input type='text' name='alt' id='alt'required placeholder='alt' \/>";
	if (item == "site") formStr += "    <br \/>";
	formStr += "	  <input type='hidden' name='item' id='item' value='"+item+"' required placeholder='item'\/>";
	formStr += "	  <input type='hidden' name='lng' id='lng' required placeholder='lng'\/>";
	formStr += "	  <input type='hidden' name='lat' id='lat' required placeholder='lat'\/>";
	formStr += " <input type= 'Submit' class='button' \/>";
	formStr += "  <\/div>";
//	formStr += "<a href=\"#\" id=\"addLanding\">Add landing...<\/a><\/div>";
	formStr += " <\/fieldset>";
	formStr += " <\/form>";
	formStr += "<br/><a onclick='cancelAddNewItem();'>Cancel</a>";
	
	var popupZoom = 'please first zoom in the map\nto the place you want the new '+item+' to be...<br/><a onclick="cancelAddNewItem();">Cancel</a>';
	var popupDrag = '...then drag me to the right place...<br/><a onclick="cancelAddNewItem();">Cancel</a>';
	
	
  	
  	/* ******   display appropriate popup when marker is created ***********/
  	if (map.getZoom() < 12) newItemMarker.bindPopup(popupZoom).openPopup();
  	else newItemMarker.bindPopup(popupDrag).openPopup();
  	
  	
  	
 	/* ******   display apropriate popup when map is zoomed ***********/
	map.on('zoomend', function(){
		newItemMarker.setLatLng([map.getCenter().lat, map.getCenter().lng]);
		if (map.getZoom() < 12) newItemMarker.setPopupContent(popupZoom);
		else newItemMarker.setPopupContent(popupDrag);
		
		newItemMarker.openPopup();
	});
 
 
 	/* ******   display apropriate popup when marker is draged ***********/
  	newItemMarker.on('dragend', function(){
		if (map.getZoom() < 12)  {
			newItemMarker.setPopupContent(popupZoom);
			newItemMarker.openPopup();
			console.log("zoom more ?");
		}
		else { 
			
			console.log("item form ?");
						
			var latNew = newItemMarker.getLatLng().lat;
			var lngNew = newItemMarker.getLatLng().lng;
			
console.log( "not yet" );
			/*
			$.getJSON('http://api.geonames.org/findNearbyJSON?lat='+latNew+'&lng='+lngNew+'&username=raf', function() {
					console.log( "success" );
				})
				.done( function( data ) {
					
					newItemMarker.setPopupContent(formStr);
					newItemMarker.openPopup();

					$("#lng").val(lngNew);
					$("#lat").val(latNew);
					
					
					if (item=='site') $('#name').val( data.geonames[0].toponymName );
					else $('#cityNewItem').val( data.geonames[0].toponymName );
					
					$('#iso').val(data.geonames[0].countryCode.toLowerCase());
					
					if (item=='site') {
						$.getJSON('http://api.geonames.org/srtm1JSON?lat='+latNew+'&lng='+lngNew+'&username=raf', function( data ) {
							$('#alt').val(data.srtm1);
						});
					}					
				});	
				*/
				
				$.getJSON('https://nominatim.openstreetmap.org/reverse?format=json&email=raphael@disroot.org&accept-language=en&lat='+latNew+'&lon='+lngNew, function() {
					
				})
				.done( function( data ) {
					console.log( "success" );
					var place;
					if (data.address["village"]) place = data.address["village"];
					if (data.address["city"])    place = data.address["city"];
					if (data.address["town"])    place = data.address["town"];
					console.log(data);
					
					
					newItemMarker.setPopupContent(formStr);
					newItemMarker.openPopup();

					$("#lng").val(lngNew);
					$("#lat").val(latNew);
					
					
					if (item=='site') $('#name').val( place );
					else $('#cityNewItem').val( place );
					
					$('#iso').val(data.address["country_code"]);
					
					if (item=='site') {
//						$.getJSON('https://api.open-elevation.com/api/v1/lookup?locations='+latNew+','+lngNew, function( data ) {
//							$('#alt').val(data.results[0].elevation);
							
						$.getJSON('https://secure.geonames.org/srtm1JSON?lat='+latNew+'&lng='+lngNew+'&username=raf', function( data ) {
							$('#alt').val(data.srtm1);
							console.log(data);
						});
					}
					
				});	
				
		}
	});
}

function geoInfo(lat, lng, item) {
	
	console.log(item);
	var city, place, iso, alt;
	
	$.getJSON('https://nominatim.openstreetmap.org/reverse?format=json&accept-language=en&lat='+latNew+'&lon='+lngNew, function( data ) {
		if (data.address["village"]) place = data.address["village"];
		if (data.address["city"])    place = data.address["city"];
		if (data.address["town"])    place = data.address["town"];
		console.log(place);
		city = place;
		iso = data.address["country_code"];
	});
	
		$.getJSON('https://secure.geonames.org/srtm1JSON?lat='+latNew+'&lng='+lngNew+'&username=raf', function( data ) {
			$('#alt').val(data.srtm1);
	});
	
	console.log(city+'---');					
	console.log(iso);		
	var result = {city, iso};			
	return result;
}


function cancelAddNewItem() {
		map.removeLayer(newItemMarker);
		map.removeLayer(newItemPopup);
	}


function submitForm(){
	console.log('submitted !');
	var data = $("#newForm").serialize();
	console.log(data);
    var item = $('#item').val();
    if (item == 'site' ) fileName = "siteNewSave.php";
    else fileName = "clubOrProNewSave.php";
	var request = $.post("../assets/ajax/updateItem/"+fileName, data, function(data){
				//console.log(data);
			})
			  .done(function(dataBack) {
				console.log( dataBack.query );
				console.log( dataBack.itemID );
				console.log( dataBack.status );
				if (dataBack.status=='success') window.location = "?"+item+"="+dataBack.itemID;
			  })
			  .fail(function() {
				console.log( "error" );
			  });
} 
