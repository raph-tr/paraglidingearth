<html>
	<head>
		<link href="../css/fa/css/all.min.css" rel="stylesheet">
		<link href="../css/fa/css/v4-shims.min.css" rel="stylesheet">
		<style>
		#content {
			display: none;
		}
		#timeRange, #colorLine, #timeLabels {
			width: 100%;
		}
		#colorLine, .colorDash {
			height: 13px;
		}
		.colorDash {
			display: inline-block;
		}
		#colorLine {
			top: -18px;
			position: relative;
			z-index: -1;
			left: 2px;
		}
		#timeLine{
			width: 100%;
			margin-bottom: 12px;
		}
		#timeLabels{
			position: relative;
		}
		.timeLabel {
			position: absolute;
			top: 8px;
			transform: rotate(-90deg);
			transform-origin: left top 0;
			font-size: x-small;
			font-weight: bold;
		}
		.flyable { color: green; }
		.unflyable { color: red; }
		</style>
	</head>
	<body>
		<div id="loading">
			<i class="fas fa-circle-notch fa-spin"></i> loading...
		</div>
		
		<div id="content">
			<div id="timeLine">
				<input id="timeRange" type="range">
				<div id="colorLine"></div>
				<div id="timeLabels"></div>
			</div>
			
			
			<div id="siteName"></div>
			<div id="reasonIcon"></div>
			<div id="rangeDate"></div>

			<div id="flyability"></div>
			<div id="reason"></div>

			<div id="rangeForecast">
				<div id="windSpeed"></div>
				<div id="windDir"></div>
				<div id="rain"></div>
				<div id="cloudbase"></div>
			</div>
		</div>

		
		
		<script src="localhost/jquery-2.1.4.min.js"></script>
		<script>
			site = <?php echo $_GET['site'];?>;
			dates = [];
			previousDay = -12; // dummy day
			i = 0;
			now = new Date();
			searchNow = true;
			
			// we query the forecasts :
			$.getJSON( "../ajax/flyable/forecasts/"+site+".json", function( data ) {
				if (data['orientations'] != "00000000" ) {
					forecasts = data['forecasts'];
					nbForecasts = Object.keys(forecasts).length;
					console.log(nbForecasts);
					$("#loading").css("display","none");
					$("#siteName").html(data['site']);
					$("#content").css("display","block");
					$.each( forecasts, function( date, forecast ) {
						
						dates.push(date);	// keep each date in an array 
						d = new Date(date);	// get a JS date
						
						// where to put the range cursor ??
						if (searchNow && now < d) {
							nowIndex = i;
							showTextForecast( date );
							searchNow=false;
						}
						
						currentForecast = forecast[date];
						
						// show range labels for days
						currentDay = d.getDay();
						if (currentDay != previousDay) {
							$("#timeLabels").append("<span class='timeLabel' id='timeLabel"+i+"'>"+d.toLocaleString([],{weekday:'short'})+"</span>");
							$("#timeLabel"+i).css("left", (parseInt(100*i/(nbForecasts-0.5)))+"%");
						}
						
						
						
						// Build colored line
						$("<span id='dash"+i+"' class='colorDash'></span>").appendTo("#colorLine");
						if (forecasts[date]['flyability'] == 100 ) $("#dash"+i).css("background","lightgreen");
						else $("#dash"+i).css("background","orange");
						
						
						i++;
						previousDay = currentDay;
						
					});
					
					
					
					//range of the range input
					document.getElementById("timeRange").max = dates.length - 1;
					document.getElementById("timeRange").value = nowIndex;
					$(".colorDash").css("width", 100/(dates.length-1)+"%");
					$("#dash0").css("width", 50/(dates.length-1)+"%");
					//lastDash = dates.length-1
					$("#dash"+ (dates.length-1)).css("width", 50/(dates.length-1)+"%");
					
					//display the information as selected date changes
					$("#timeRange").on("change mousemove", function() {
						thisDate = dates[$(this).val()];
						showTextForecast( thisDate );
						
					});
				} else {
					$("#siteName").html(data['site']+" : no site orientations known..  :(");
					$("#loading").css("display","none");
				}
			})
			.fail(function() {
				$.get( "../ajax/flyable/import_weather.php?site="+site, function() {
					setTimeout(function(){
					//	location.reload();
					}, 500);
				});
			});
			
			function showTextForecast( thisDate ) {
					thisDateDate = new Date( thisDate );
					dateOptions = options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute:'2-digit' };
					$("#rangeDate").html( thisDateDate.toLocaleString([],options) );
					$("#flyability").html("flyability : "+forecasts[thisDate]['flyability']+"%");
					if (forecasts[thisDate]['flyability'] < 100 ) $("#reason").html("reason : "+forecasts[thisDate]['reason']+"");
					else $("#reason").html("it seems fine");
					$("#windSpeed").html("<i class='fas fa-wind'></i> wind : "+forecasts[thisDate]['windSpeed']+" m/s");
					$("#windDir").html("<i class='fas fa-compass'></i> windDir : "+forecasts[thisDate]['windDir']+"°");
					$("#rain").html("<i class='fas fa-cloud-showers-heavy'></i> rain : "+forecasts[thisDate]['rain']+"");
					$("#cloudbase").html("<i class='fas fa-cloud'></i> cloudbase : "+forecasts[thisDate]['cloudbase']+" m");
					if (forecasts[thisDate]['flyability'] == 100 ) {
						$("#flyability, #reason").addClass("flyable").removeClass("unflyable");
						$("#reasonIcon").html('<i class="far fa-thumbs-up flyable"></i>');
					} else { 
						$("#flyability, #reason").addClass("unflyable").removeClass("flyable");;
						$("#reasonIcon").html("");
					}
						if (forecasts[thisDate]['reason'].includes("windSpeed")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-wind"></i>');
							$("#windSpeed").addClass("unflyable").removeClass("flyable"); 
						} else { $("#windSpeed").addClass("flyable").removeClass("unflyable"); }
						if (forecasts[thisDate]['reason'].includes("windDir")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-compass"></i>');
							$("#windDir").addClass("unflyable").removeClass("flyable"); 
						} else { $("#windDir").addClass("flyable").removeClass("unflyable"); }
						if (forecasts[thisDate]['reason'].includes("rain")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-cloud-showers-heavy"></i>');
							$("#rain").addClass("unflyable").removeClass("flyable");
						} else { $("#rain").addClass("flyable").removeClass("unflyable"); }
						if (forecasts[thisDate]['reason'].includes("cloudbase")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-cloud"></i>');
							$("#cloudbase").addClass("unflyable").removeClass("flyable");
						} else { $("#cloudbase").addClass("flyable").removeClass("unflyable"); }
			}
			
		</script>
	</body>
</html>
