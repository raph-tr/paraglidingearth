<!DOCTYPE html>
<html>
	<head>
		
		<title>Import</title>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
	    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
	    <link rel="stylesheet" href="css.css">



	</head>

	<body>

		<div id='map'></div>

		<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin=""></script>
	    <script src="d3.js"></script>
	    <script src="../assets/js/localhost/jquery-2.1.4.min.js"></script>
		<script src="js.js"></script>
		<script src="countriesBorder.js"></script>

	</body>
</html>
