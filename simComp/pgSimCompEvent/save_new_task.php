<?php
session_start();
include "../connexion.php";
include "../pgearth/secure.php";
?>

<?php
$site_id = $_POST['site_id'];
$member_id = $_POST['member_id'];
$name = addslashes($_POST['task_name']);
$description = addslashes($_POST['task_description']);
$event_id = $_POST['event_id'];
$task_length = $_POST['task_length'];
$test_mode = $_POST['test_mode'];
$expires = $_POST['expires'];
$starts = $_POST['starts'];

for ($i=1; $i<7; $i++){
	$lat[$i] = NULL;	
	$lng[$i] = NULL;	
	$d[$i]   = '';	
}

for ($i=1; $i<= $_POST['last_active_waypoint']; $i++){
	$lat[$i] = $_POST['lat_b'.$i];
	$lng[$i] = $_POST['lng_b'.$i];
	$d[$i]   = $_POST['distance_b'.$i];
}

$wind_direction = $_POST['wind_direction'];
$wind_speed =  $_POST['wind_speed'];

$thermals_density = $_POST['thermal_density'];
$thermals_height = $_POST['thermal_height'];
$thermals_size = $_POST['thermal_size'];
$thermals_speed = $_POST['thermal_speed'];

/*-------------------------------------------------------------------
--------------------------------------------------------------------*/

$query = "INSERT INTO `pge`.`pgsim_comp_tasks` (
`id` , `task_name` , `task_description` , `competition` ,`task_length` , `is_pge_challenge` , `site_id` , `starts`, `expires` , `testing` , `organised_by` ,
`lng_b1` , `lat_b1` , `distance_b1` ,
`lng_b2` , `lat_b2` , `distance_b2` ,
`lng_b3` , `lat_b3` , `distance_b3` ,
`lng_b4` , `lat_b4` , `distance_b4` ,
`lng_b5` , `lat_b5` , `distance_b5` ,
`lng_b6` , `lat_b6` , `distance_b6` ,
`wind_direction` , `wind_speed` ,
`thermals_height` , `thermals_width` , `thermals_density` , `thermals_speed`
)
VALUES (
NULL , '$name', '$description', '$event_id', '$task_length', '0', '$site_id', '$starts', '$expires', '$test_mode', '$member_id',
'$lng[1]', '$lat[1]', '$d[1]',
'$lng[2]', '$lat[2]', '$d[2]',
'$lng[3]', '$lat[3]', '$d[3]',
'$lng[4]', '$lat[4]', '$d[4]',
'$lng[5]', '$lat[5]', '$d[5]',
'$lng[6]', '$lat[6]', '$d[6]',
'$wind_direction', '$wind_speed',
'$thermals_height', '$thermals_size', '$thermals_density', '$thermals_speed'
);";

if ($result= mysql_query($query)){
	?>
	<script>window.location.replace('events.php?event=<?php echo $_POST['event_id'];?>');</script>
	<?php
} else {
	echo "oops, a problem occured, please hit the 'back' button and try again...";	
}
?>
