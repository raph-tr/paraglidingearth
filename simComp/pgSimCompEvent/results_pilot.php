<?php
session_start();
include "../connexion.php";

$yearNow=date('Y');
$year = $yearNow;
if (isset($_GET['year'])) $year = $_GET['year'];


function secondsToHoursMinutesSeconds($time){
if ($time < 86400 AND $time>=3600)
// si le nombre de secondes ne contient pas de jours mais contient des heures
{
// on refait la même opération sans calculer les jours
$heure = floor($time/3600);
$reste = $time%3600;
$minute = floor($reste/60);
$seconde = $reste%60;
$result = $heure.'h.'.$minute.'min.'.$seconde.'s.';
}
elseif ($time<3600 AND $time>=60)
{
// si le nombre de secondes ne contient pas d'heures mais contient des minutes
$minute = floor($time/60);
$seconde = $time%60;
$result = $minute.'min.'.$seconde.'s.';
}
elseif ($time < 60)
// si le nombre de secondes ne contient aucune minutes
{
$result = $time.'s.';
}
return $result;
}


$pilot_id = $_GET['pilot_id'];

$sql = 'SELECT count(distinct task_id) as tasks, count(pgsim_comp_results.id) as flights, sum(first_try_score) as points, pseudo 
FROM `pgsim_comp_results`
 left join auteur on pilot_id = id_auteur
 LEFT  JOIN pgsim_comp_tasks ON pgsim_comp_tasks.id = task_id
 WHERE `pilot_id` = '.$pilot_id.' 
 and not cancelled
 and pgsim_comp_results.competition<>"open"
 and testing = 0
 GROUP BY pilot_id' ;
$que = mysql_query($sql) ;
$val = mysql_fetch_array($que) ;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script type="text/javascript" src="../scripts/overlib.js"></script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>Personnal results</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="generator" content="HAPedit 3.1">
</head>
<body>

<h1>Individual pilot results</h1>

<?php
if ($pilot_id==$_SESSION['id_membre']) $here = "results_pilot" ;
else $here="results_overall"; 
include "tabs_header.php";
?>
	
<div class="rubriqueSite">
<div class="titreMenu">
<?php echo $val['pseudo']." scored a total of ".$val['points']." points. This took him ".$val['flights']." flights in ".$val['tasks']." tasks to achieve that.";?>
</div>

Let's have a look at it in details :<br />
&nbsp;

  <link href="../pgearth/style/style_tabs.css" rel="stylesheet" type="text/css">

  <div id="tabs">
      <ul>
<?php
for ($i=2008; $i<=$yearNow; $i++){
?>		<li <?php if($i==$year) echo "class=\"here\"";?>><a href="?pilot_id=<?php echo $pilot_id;?>&year=<?php echo $i;?>"><?php echo $i?></a></li>
<?php
}
?>
      </ul>
  </div>
  <div id="content">

<table>
  <tr align="center">
     <td>Points</td>
     <td>Task</td>
     <td>Distance (km)</td>
     <td>Flight time</td>
  </tr>
<?php

 $sql_res = 'SELECT task_id, max(distance) as distance, flight_duration, max(score) as score FROM `pgsim_comp_results` WHERE `pilot_id` = '.$pilot_id.'  and pgsim_comp_results.competition<>"open"
 and year(date)='.$year.' and not cancelled group by task_id order by score desc';
 $res_res = mysql_query($sql_res);
 
 while ($val_res = mysql_fetch_array($res_res)){
   $sql_task = 'select id, testing, task_name, task_length, iso, nom_pays_en from pgsim_comp_tasks 
		left join site on site_id=id_site left join pays on pays=id_pays where id='.$val_res['task_id'] ;
   $res_task = mysql_query($sql_task);
   $val_task = mysql_fetch_array($res_task);

   $sql_best_flight = 'select flight_duration from pgsim_comp_results where task_id='.$val_task['id'].' and  and pgsim_comp_results.competition<>"open"
   year(date)='.$year.' and not cancelled and pilot_id = '.$pilot_id.' order by score desc limit 1';
   $res_best_flight = mysql_query($sql_best_flight);
   $val_best_flight = mysql_fetch_array($res_best_flight);
   
  $sql_all_flights = 'select id from pgsim_comp_results where task_id='.$val_task['id'].'  and pilot_id='.$pilot_id.'  and pgsim_comp_results.competition<>"open"
  and year(date)='.$year.' and  not cancelled';
   $res_all_flights = mysql_query($sql_all_flights);
   $num_all_flights = mysql_num_rows($res_all_flights);

   if ($val_res['distance'] == 999999) $distance = "completed";
   else $distance = $val_res['distance']/1000 ."  / ". $val_task['task_length']/1000;
   if ($val_task['testing'] == 1) $testing_img = "../pgearth/images/famfamfamicons/flag_orange.png";
   else $testing_img = "../pgearth/images/famfamfamicons/flag_green.png";
 ?>
  <tr align="center">
<?php  echo "<td>".$val_res['score']."</td>
<td><b>".$val_task['task_name']." - <img src=\"../pgearth/images/drapeaux/_tn_".strtolower($val_task['iso']).".png\"></b></td>
<td>". $distance." </td><td>".secondsToHoursMinutesSeconds($val_best_flight['flight_duration'])."</td>
</tr>";

 } ?>
</table> 
   </div>
</div>
</body>
</html>
