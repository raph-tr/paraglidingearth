<?php
$postfields = array();
$postfields["MAX_FILE_SIZE"] = "1000000";
$postfields["pilot"] = "Pilot Name";
$postfields["date"] = "24/05/2010";
$postfields["timeshift"] = "+3";
$postfields["departure"] = "takeoff name";
$postfields["arrival"] = "arrival name";
$postfields["userfile"] = "http://www.paraglidingearth.com/pgSimCompEvent/igc/event3/task3/raf.igc";
$postfields["Charts"] = "on";
$postfields["cfd"] = "off";
$postfields["Analysis"] = "off";
$postfields["choix"] = "on";

//url de la page de soumission
//ici mon annuaire (qui a un captcha donc inutile de tenter la soumission auto, c'est pour l'exemple)
$url = "http://trace.parawing.net/get_form_gps2ge.php";
$useragent = "Mozilla/5.0";
$referer = $url; 
 
//Initialise une session CURL
$ch = curl_init($url);
//CURL options
curl_setopt($ch, CURLOPT_POST, 1);
//On poste les donn�es du tableau $postfields
curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
//On d�finit un useragent ici Mozilla/5.0
curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
//On passe un referrer ici on passe la m�me page $url
curl_setopt($ch, CURLOPT_REFERER, $referer);
//on r�cup�re le contenu de la page de r�sultat de la soumission dans une chaine
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// en cas de redirection (facultatif ici)
if (curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1)) echo "good";
//Page de r�sultats et fermeture de session
$result = curl_exec($ch);
curl_close($ch);
 
//on peut faire un echo du r�sultat obtenu
//echo $result;
?>