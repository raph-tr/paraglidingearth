<?php
require_once "graph/Pie.class.php";
include "../connexion.php";


$query = "SELECT distinct pilot_id , count(id )as number FROM `pgsim_comp_results` WHERE competition =".$_GET['event_id']." group by pilot_id   ORDER BY `number` asc";
$result= mysql_query($query);
$num_pilots = mysql_num_rows($result);

$i=0;
$values[0] = 0;
$legend[0] = 'flew 1 task';

while ($val = mysql_fetch_array($result)){
    if ($val['number'] == $i+1)  {
        $values[$i]++;
    } else {
        $i++;
        $values[$i]=1;
        $j=$i+1;
        $legend[$i]= 'flew '.$j.' tasks';
    }
}


$graph = new Graph(300, 200);

$graph->title->set($num_pilots." pilots took part in the event");

$plot = new Pie($values, PIE_EARTH);
$plot->setCenter(0.3, 0.55);
$plot->setSize(0.52, 0.58);
$plot->set3D(11);

$plot->setLegend($legend);
$plot->legend->setPosition(1.78, 0.7);
$plot->legend->setSpace(2);
$plot->legend->setTextMargin(2,2);
$plot->legend->shadow->setSize(1);

$graph->add($plot);
$graph->draw();
?>