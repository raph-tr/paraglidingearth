<?php
$queryExpired = 'select timediff(now(), expires) as expired, datediff(now(), expires) as expiredInDays  from pgsim_comp_tasks where id = '.$task_id;
$valExpired = mysql_fetch_array(mysql_query($queryExpired));

if ($valExpired['expired'] < 0 ){

   echo "<div class='rubriqueSite'><b>Results of this task will be available when the task has expired, which is in ".-$valExpired['expiredInDays']." days.</b></div>";

} else { 
?>

<div class="rubriqueSite">
  <b>Actual results on the task :</b>


<?php

$show_result_tab = "first_flight";
if (isset($_GET['show_result'])) $show_result_tab = $_GET['show_result'];


function secondsToHoursMinutesSeconds($time){
if ($time < 86400 AND $time>=3600)
// si le nombre de secondes ne contient pas de jours mais contient des heures
{
// on refait la m�me op�ration sans calculer les jours
$heure = floor($time/3600);
$reste = $time%3600;
$minute = floor($reste/60);
$seconde = $reste%60;
$result = $heure.'h.'.$minute.'min.'.$seconde.'s.';
}
elseif ($time<3600 AND $time>=60)
{
// si le nombre de secondes ne contient pas d'heures mais contient des minutes
$minute = floor($time/60);
$seconde = $time%60;
$result = $minute.'min.'.$seconde.'s.';
}
elseif ($time < 60)
// si le nombre de secondes ne contient aucune minutes
{
$result = $time.'s.';
}
return $result;
}
?>
<div id="tabs">
    <ul>
      <li <?php if($show_result_tab=="first_flight") echo "class=\"here\"";?>><a href="?show_result=first_flight&task_id=<?php echo $task_id; ?>">First Attempt ranking</a></li>
      <li <?php if($show_result_tab=="all_flights") echo "class=\"here\"";?>><a href="?show_result=all_flights&task_id=<?php echo $task_id; ?>">Best Attempt ranking</a></li>
    </ul>
</div>

<?php

$qTask = "select task_length from pgsim_comp_tasks where id = ".$task_id;
$vTask = mysql_fetch_array(mysql_query($qTask));
$task_length = $vTask['task_length'];


if ($show_result_tab=="first_flight"){

$queryResults = "SELECT min(pgsim_comp_results.date) as date, id, pilot_id, distance, cancelled, first_try_score, flight_duration, pseudo
FROM `pgsim_comp_results` LEFT JOIN auteur ON id_auteur=pilot_id
WHERE `task_id` =".$task_id." and not cancelled
group by pilot_id 
ORDER BY distance DESC, flight_duration ASC";

$resultResults = mysql_query($queryResults);
$numResults = mysql_num_rows($resultResults);
if ($numResults > 0) {
	$rank = 0;
?>
<div id="content">
  <table border="1">
   <tr>
	<td align="center">Ranking</td>
	<td align="center">Score</td>
	<td align="center">Pilot</td>
	<td align="center">Distance (km)</td>
	<td align="center">Flight duration</td>
	<td align="center">Speed (km/h)</td>
	<td align="center">Date</td>
	<td align="center">View flight</td>
   </tr>	
<?php
    $oneResultPerPilot = array();

	while ($valResults=mysql_fetch_array($resultResults)){
		
	if ($valResults['cancelled'] or in_array($valResults['pilot_id'], $oneResultPerPilot)) {
		$isCountable = 0;
		$notBestResult = 1;
	} else {
		array_push($oneResultPerPilot, $valResults['pilot_id']);
		$notBestResult=0;
		$rank++;
		$isCountable = 1;
	}
	
	?>
	<tr<?php if (isset($showTaskId) and $showTaskId==$valResults['id']) echo " style=\"background: red;\"";
	if($valResults['cancelled']) echo " style=\"text-decoration: line-through; background: lightgray;\"";
	if($notBestResult) echo " style=\"color: gray; background: beige;\"";
	?>>
	 <td align="center"><?php
	 if ($isCountable==0) {
		 echo "X";
	 } else {
		if ($rank==1) {echo "<img src='../pgearth/images/famfamfamicons/medal_gold_1.png' />";}
		elseif ($rank==2) {echo "<img src='../pgearth/images/famfamfamicons/medal_silver_2.png' />";}
		elseif ($rank==3) {echo "<img src='../pgearth/images/famfamfamicons/medal_bronze_3.png' />";}
		else echo $rank;
	 }
	?>
	</td>
	 <td align="center"><?php if ($isCountable) echo $valResults['first_try_score']; else echo "X";?></td>
	 <td align="center"><a href="results_pilot.php?pilot_id=<?php echo $valResults['pilot_id'];?>" title="view pilot full results"><?php echo $valResults['pseudo'];?></a></td>
	 <td align="center"><?php if ($valResults['distance']==999999)echo "goal reached"; else {echo $valResults['distance']/1000;}?></td>
	 <td align="center"><?php echo secondsToHoursMinutesSeconds($valResults['flight_duration']);?></td>
	 <td align="center"><?php if ($valResults['distance']==999999) {echo round(3.6*$task_length/$valResults['flight_duration'],2);} else {echo round(3.6*$valResults['distance']/$valResults['flight_duration'],2);}?></td>
	 <td align="center"><?php echo $valResults['date'];?></td>
	 <td align="center">
	 <a href="javascript:void(0)" title="view flight" onclick="return overlib('<iframe src=\'http://www.victorb.fr/visugps/visugps.html?track=http://www.paraglidingearth.com/pgSimComp/viewIGC.php?flight_id=<?php echo $valResults['id'] ;?>\' frameborder=\'0\' height=\'325\' width=\'455\'></iframe>', STICKY, CAPTION, 'view flight', WIDTH, 460, HEIGHT, 330, HAUTO, VAUTO, FGCOLOR, '#FFFFFF', BGCOLOR, '#990000');" onmouseout="">
	 <img src="../pgearth/images/famfamfamicons/magnifier.png" border="0" /></a></td>
   </tr>	
<?php 	
    }
  echo "</table>
</div>";
}
 else {
   echo "<div>no results yet, be the first one to run the task and take the lead !</div>";
 }
}


if ($show_result_tab=="all_flights"){


$queryResults = "SELECT DATE(pgsim_comp_results.date) as date, id, pilot_id, distance, cancelled, score, flight_duration, pseudo FROM `pgsim_comp_results` LEFT JOIN auteur ON id_auteur=pilot_id WHERE `task_id` =".$task_id." ORDER BY distance DESC, flight_duration ASC";
$resultResults = mysql_query($queryResults);
$numResults = mysql_num_rows($resultResults);
if ($numResults > 0) {
	$rank = 0;
?>
<div id="content">
 <table border="1">
   <tr>
	<td align="center">Ranking</td>
	<td align="center">Score</td>
	<td align="center">Pilot</td>
	<td align="center">Distance (km)</td>
	<td align="center">Flight duration</td>
	<td align="center">Speed (km/h)</td>
	<td align="center">Date</td>
	<td align="center">View flight</td>
   </tr>	
<?php
    $oneResultPerPilot = array();

	while ($valResults=mysql_fetch_array($resultResults)){
		
	if ($valResults['cancelled'] or in_array($valResults['pilot_id'], $oneResultPerPilot)) {
		$isCountable = 0;
		$notBestResult = 1;
	} else {
		array_push($oneResultPerPilot, $valResults['pilot_id']);
		$notBestResult=0;
		$rank++;
		$isCountable = 1;
	}
	
	?>
	<tr<?php if (isset($showTaskId) and $showTaskId==$valResults['id']) echo " style=\"background: red;\"";
	if($valResults['cancelled']) echo " style=\"text-decoration: line-through; background: lightgray;\"";
	if($notBestResult) echo " style=\"color: gray; background: beige;\"";
	?>>
	 <td align="center"><?php
	 if ($isCountable==0) {
		 echo "X";
	 } else {
		if ($rank==1) {echo "<img src='../pgearth/images/famfamfamicons/medal_gold_1.png' />";}
		elseif ($rank==2) {echo "<img src='../pgearth/images/famfamfamicons/medal_silver_2.png' />";}
		elseif ($rank==3) {echo "<img src='../pgearth/images/famfamfamicons/medal_bronze_3.png' />";}
		else echo $rank;
	 }
	?>
	</td>
	 <td align="center"><?php if ($isCountable) echo $valResults['score']; else echo "X";?></td>
	 <td align="center"><a href="results_pilot.php?pilot_id=<?php echo $valResults['pilot_id'];?>" title="view pilot full results"><?php echo $valResults['pseudo'];?></a></td>
	 <td align="center"><?php if ($valResults['distance']==999999)echo "goal reached"; else {echo $valResults['distance']/1000;}?></td>
	 <td align="center"><?php echo secondsToHoursMinutesSeconds($valResults['flight_duration']);?></td>
	 <td align="center"><?php if ($valResults['distance']==999999) {echo round(3.6*$task_length/$valResults['flight_duration'],2);} else {echo round(3.6*$valResults['distance']/$valResults['flight_duration'],2);}?></td>
	 <td align="center"><?php echo $valResults['date'];?></td>
	 <td align="center">
	 <a href="javascript:void(0)" title="view flight" onclick="return overlib('<iframe src=\'http://www.victorb.fr/visugps/visugps.html?track=http://www.paraglidingearth.com/pgSimComp/viewIGC.php?flight_id=<?php echo $valResults['id'] ;?>\' frameborder=\'0\' height=\'325\' width=\'455\'></iframe>', STICKY, CAPTION, 'view flight', WIDTH, 460, HEIGHT, 330, HAUTO, VAUTO, FGCOLOR, '#FFFFFF', BGCOLOR, '#990000');" onmouseout="">
	 <img src="../pgearth/images/famfamfamicons/magnifier.png" border="0" /></a></td>
   </tr>	
<?php 	
    }
  echo "</table>
</div>";
}
 else {
   echo "<div>no results yet, be the first one to run the task and take the lead !</div>";
 }
}

}
?>

   </div>
