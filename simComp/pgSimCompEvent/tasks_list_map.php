
<table width="100%">
<tr>
<td width="604px">
<div id="map_canvas" style="width: 600px; height: 400px"></div>
</td>
<td>
<div id="sidebar" style="height:400px; overflow:auto">
<div id="vertical-accordion" class="rubriqueSite">
<?php 
while ($valByCountries = mysql_fetch_array($resultByCountries)){
	$queryTasks = "select * from pgsim_comp_tasks 
	left join site on site_id=id_site
	left join pays on pays = id_pays
	where id_pays = ".$valByCountries['id_pays']."
	order by task_length";
	$resultTasks= mysql_query($queryTasks);
	$numTasks= mysql_num_rows($resultTasks);
	?>

<div id="<?php echo $valByCountries['nom_pays_en']; ?>-header" class="accordion_toggle" >
<img src="../pgearth/images/drapeaux/_tn_<?echo strtolower($valByCountries['iso']);?>.png" /> - <?php echo $valByCountries['nom_pays_en']; ?> (<?php echo $numTasks ?>  task<?php if ($numTasks>1) echo "s";?>)
</div>
<div class="accordion_content">
<table>
	<tr>
		<td align="center"><b>view</b></td>
		<td align="center"><b>length</b></td>
		<td align="center"><b>flights</b></td>
		<td align="center"><b>completed</b></td>
		<td align="center"><b>status</b></td>
	</tr>
<?php while ($vTasks=mysql_fetch_array($resultTasks)){
		
		if ($vTasks['task_name']=="") $taskName = "no name...";
		else $taskName = $vTasks['task_name'];
		
		$qSite = "select nom, id_site, iso from site left join pays on site.pays=id_pays where id_site = ".$vTasks['site_id'];
		$vSite = mysql_fetch_array(mysql_query($qSite));
		
		$qResults = "select * from pgsim_comp_results where task_id = ".$vTasks['id'];
		$vResults = mysql_fetch_array(mysql_query($qResults));
		$nResults = mysql_num_rows(mysql_query($qResults));
		$qCompleted = "select * from pgsim_comp_results where task_id = ".$vTasks['id']." and distance = 999999 order by flight_duration asc";
		$vCompleted = mysql_fetch_array(mysql_query($qCompleted));
		$nCompleted = mysql_num_rows(mysql_query($qCompleted));
?>
	<tr>
		<td align="center"><a title="Site : <?php echo stripslashes($vSite['nom']);?>" href="javascript:void(0);" onClick="javascript:showTaskOnMap(marker<?php echo $vTasks['id'];?>, infoWindow<?php echo $vTasks['id'];?>, taskPolyline<?php echo $vTasks['id'];?>);"><?php echo $taskName;?></a></td>
		<td align="center"><?php echo  round($vTasks['task_length']/1000) ?> km</td>
		<td align="center"><?php echo  $nResults ?></td>
		<td align="center"><?php echo  $nCompleted ?></td>
		<td align="center"><?php if ($vTasks['testing']) {$flag ="orange"; $title="task in testing mode";} else {$flag="green"; $title="task in definitive version";}  ?>
		<img src="../pgearth/images/famfamfamicons/flag_<?php echo $flag;?>.png" title="<?php echo $title;?>" /></td>		
	</tr>
<?php
	}
?>
</table>
</div>

<?php }
?>
</div>

</div>
</td>
</tr>
</table>
<script>
if (GBrowserIsCompatible()) {

	var currentlyShownPolyline = new GPolyline();

    var map = new GMap2(document.getElementById("map_canvas"));
    var center = new GLatLng(0, 0);
    map.setCenter(center, 1);
	map.addControl(new GSmallMapControl());
	map.addControl(new GHierarchicalMapTypeControl());
	map.addMapType(G_SATELLITE_3D_MAP);
	map.addMapType(G_PHYSICAL_MAP);
	map.setMapType(G_PHYSICAL_MAP);

    function createMarker(lat, lng, infoWindow, polyline){
		var marker = new GMarker(new GLatLng(lat, lng));
		GEvent.addListener(marker, "click", function() {
			showTaskOnMap(marker, infoWindow, polyline)
		});
		map.addOverlay(marker);
		return marker;
	}

	function showTaskOnMap(marker, text, polyline){
		marker.openInfoWindowHtml(text);
		map.setCenter(marker.getLatLng());
		currentlyShownPolyline.hide();
		polyline.show() ;
		currentlyShownPolyline = polyline ;
		map.setZoom(11);	
	}	
	
<?php 
	$r = mysql_query("select * from pgsim_comp_tasks left join site on id_site=site_id");
	while ($vTasks=mysql_fetch_array($r)){
		if($vTasks['latd']<>0 or $vTasks['longd']<>0) {
			
			if ($vTasks['task_name']=="") $taskName = "no name...";
			else $taskName = $vTasks['task_name'];
			
			$qResults = "select * from pgsim_comp_results where task_id = ".$vTasks['id'];
			$vResults = mysql_fetch_array(mysql_query($qResults));
			$nResults = mysql_num_rows(mysql_query($qResults));
			$qCompleted = "select * from pgsim_comp_results where task_id = ".$vTasks['id']." and distance = 999999 order by flight_duration asc";
			$vCompleted = mysql_fetch_array(mysql_query($qCompleted));
			$nCompleted = mysql_num_rows(mysql_query($qCompleted));
			
			$polyLine = "new GPolyline([";
			$polyLine .= "
		new GLatLng(".$vTasks['latd'].", ".$vTasks['longd']."),";
			for ($i=1 ; $i<7; $i++){
				if ($vTasks['lat_b'.$i]<>0 or $vTasks['lng_b'.$i]<>0 ) $polyLine .= "
		new GLatLng(".$vTasks['lat_b'.$i].", ".$vTasks['lng_b'.$i]."),";
			}
			$polyLine = substr($polyLine, 0, -1)."
		], \"#ff0000\", 10)";
		
		if ($vTasks['testing']) {$flag ="orange"; $title="task in testing mode";}
		else {$flag="green"; $title="task in definitive version";}
	
			echo "
		infoWindow".$vTasks['id']."	= '<b>".addslashes($taskName)."</b><ul class=\'menu\'><li>Site : ".addslashes($vTasks['nom'])."</li><li>Status : <img src=\'../pgearth/images/famfamfamicons/flag_".$flag.".png\' title=\'".$title."\' /> ".$title."</li><li>Length : ". round($vTasks['task_length']/1000) ." km&nbsp;</li><li>". $nResults ." tries on this task</li><li>". $nCompleted ." times completed</li><li><a href=\"start_task.php?task_id=".$vTasks['id']."\"><b>View details / Fly the task</a></li></ul>';
	var taskPolyline".$vTasks['id']."	=  ".$polyLine." ;
	map.addOverlay(taskPolyline".$vTasks['id'].");
	taskPolyline".$vTasks['id'].".hide() ;
	marker".$vTasks['id']."	= createMarker(".$vTasks['latd'].",".$vTasks['longd'].",infoWindow".$vTasks['id'].", taskPolyline".$vTasks['id'].");\n";
		}
	}
?>
}
</script>