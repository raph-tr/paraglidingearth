<?php


?>
<html>
	<head>
		<link href="external_js/carpe_slider/default.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="external_js/carpe_slider/slider.js" ></script>
	</head>
	<body>
		<form action="" method="GET">
			<h2>Rate the task</h2>
			<div>
				How interesting/entertaining did you find the task (landscape, tactical choices, originality, etc...)
				<div class="carpe_horizontal_slider_display_combo">
					<div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
						<div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
						<!-- Default position: 80px -->
						<div
						  class="carpe_slider"
						  id="sliderInterest"
						  display="displayInterest"
						  style="left: 0px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;
						</div>
					</div>
					<div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
						<!-- Default value: 180 -->
						<input
						  class="carpe_slider_display"
						  name="interest"
						  id="displayInterest"
						  style="background-color: #fff; color: #258;"
						  type="text"
						  from="0"
						  to="5"
						  value="0" /> / 5 
					</div>
				</div>
			</div>
			<div>??</div>
			<div>
				How difficult did you find the task :
				<div class="carpe_horizontal_slider_display_combo">
					<div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
						<div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
						<!-- Default position: 80px -->
						<div
						  class="carpe_slider"
						  id="sliderDifficulty"
						  display="displayDifficulty"
						  style="left: 0px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;
						</div>
					</div>
					<div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
						<!-- Default value: 180 -->
						<input
						  class="carpe_slider_display"
						  name="interest"
						  id="displayDifficulty"
						  style="background-color: #fff; color: #258;"
						  type="text"
						  from="0"
						  to="5"
						  value="0" /> / 5
					</div>
				</div>
			</div>
		</form>
	</body>
</html>