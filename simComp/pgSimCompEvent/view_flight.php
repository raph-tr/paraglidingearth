<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">


<head>
  
     <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    
    <style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
    </style>
 

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAloePAlEicH6IDTkMBLcJrhQXwnL9FxgcTH4vf2kk7gAE15haUxSJ7gKW1yxt4vCGL_lEpmkZonN36w" type="text/javascript"></script>


<link href="../pgearth/style/style.css" rel="stylesheet" type="text/css">

<?php
include "../connexion.php";

function trad_coord($coord){
 $coord = $coord / 100000;
 $partie_entiere  = floor($coord);
 $partie_decimale = $coord - $partie_entiere;
 $partie_decimale = 10 * $partie_decimale / 6;
 $coord = $partie_entiere + $partie_decimale ;
 return round($coord,6);
}


$i=0;
$coefficient=5;

$site_id = $_POST['site_id'];
$member_id  = $_POST['member_id'];


$queryauteur="SELECT pseudo FROM auteur WHERE id_auteur = $member_id";
$resultauteur = mysql_query($queryauteur);
$valauteur = mysql_fetch_array($resultauteur);

$querysite="SELECT nom FROM site WHERE id_site = $site_id";
$resultsite = mysql_query($querysite);
$valsite = mysql_fetch_array($resultsite);



?>
    <style type="text/css">
      .tooltip {
        background-color:#ffffff;
        font-weight:bold;
        margin:2px;
        border:1px;
      }
    </style>
    
<title><?php echo $valsite['nom'];?>, flight by <?php echo $valauteur['pseudo']; ?> on PGE sim :)</title>
    
</head>
  
  <body>

  <script type="text/javascript">
//   ---- Mouse wheel zoom
function GoogleMapWheelZoom(a) {
    if(a.cancelable)    a.preventDefault();
    if ((a.detail || -a.wheelDelta) < 0) map.zoomIn();
    else map.zoomOut();        
    return false; 
}
//   ---- end of mouse wheel zoom
  </script>

<?php

$fichier_igc = $_POST['igcFile'];

$inf = explode("\n" , $fichier_igc);

echo "
 <script type=\"text/javascript\">
    var points=[];
    var bounds = new GLatLngBounds();";
    $poly =  "  var polyline = new GPolyline([";

  for($iz=0;$iz<sizeof($inf);$iz++) {
    
    $buffer = $inf[$iz];
    
    $pattern = "^[B0-9{13}]";
    $hit = ereg($pattern, $buffer, $sortie);
 
    if ($hit==1) {
    
      if ($i % $coefficient==0){
    
    $coordnord   = substr($buffer, 7, 7);
    $cardnord = substr($buffer, 14,1);
    $nord   = trad_coord($coordnord);
    if ($cardnord=="S") $nord=-1*$nord;
    
    $coordest    = substr($buffer, 15, 8);
    $cardest = substr($buffer, 23,1);
    $est    = trad_coord($coordest);
    if ($cardest=="W") $est=-1*$est;
    
    if ($i==0){$start="new GLatLng(".$nord.",".$est.")";}
    
    if ($i % (4*$coefficient)==0){
        echo "  bounds.extend(new GLatLng(".$nord.",".$est."));
";  }
    $poly .= "
    new GLatLng(".$nord.",".$est."),";
    
      }
      
      $i++;
   }

}

$poly = substr($poly, 0, -1);
$poly .= "  ], \"#ffff00\", 2);
";

$end = "new GLatLng(".$nord.",".$est.")";

echo $poly."
 
 </script>";
 
 ?>
<table>
 <tr>
  <td>
   <div id="map" style="width: 740px; height: 512px"></div>
  </td>
  <td>
      <form name="igcForm" action="save_flight.php" method="post">
        <textarea name='igcFile' style='width: 650px; height: 180px; display: none' ><?php echo $_POST['igcFile'];?></textarea>
        A comment on the flight :<br />
        <textarea name='comment' style='width: 320px; height: 100px;' ></textarea> <br />
        <input name='member_id' type="hidden" value="<?php echo $_POST['member_id'];?>" />
        <input name='site_id' type="hidden" value="<?php echo $_POST['site_id'];?>" />
        <input name='wind_speed' type="hidden" value="<?php echo $_POST['wind_speed'];?>" />
        <input name='wind_direction' type="hidden" value="<?php echo $_POST['wind_direction'];?>" />
        <input name='thermal_height' type="hidden" value="<?php echo $_POST['thermal_height'];?>" />
        <input name='thermal_size' type="hidden" value="<?php echo $_POST['thermal_size'];?>" />
        <input name='thermal_density' type="hidden" value="<?php echo $_POST['thermal_density'];?>" />
        <input name='thermal_speed' type="hidden" value="<?php echo $_POST['thermal_speed'];?>" />
        <input type="submit" value="Save this flight !" />
      </form>
        <input type="button" value="Don t save, close window" onclick="window.close();"/>
  </td>
 </tr>
</table>

  </body>
</html>


<script type="text/javascript" defer="defer">
    //<![CDATA[

    if (GBrowserIsCompatible()) { 
    
    
    //start - end markers icons
         var baseIcon = new GIcon();
          baseIcon.iconSize=new GSize(32,32);
          baseIcon.shadowSize=new GSize(56,32);
          baseIcon.iconAnchor=new GPoint(16,32);
          baseIcon.infoWindowAnchor=new GPoint(16,0);
     
      var starticon   = new GIcon(baseIcon, "http://maps.google.com/mapfiles/kml/pal5/icon13.png", null, "http://maps.google.com/mapfiles/kml/pal5/icon13s.png");
      var endicon   = new GIcon(baseIcon, "http://maps.google.com/mapfiles/kml/pal2/icon27.png", null, "http://maps.google.com/mapfiles/kml/pal2/icon27s.png");
   
      // ====== This function displays the tooltip ======
      // it can be called from an icon mousover or a sidebar mouseover
      function showTooltip(marker) {
        tooltip.innerHTML = marker.tooltip;
    var point=map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(),map.getZoom());
    var offset=map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(),map.getZoom());
    var anchor=marker.getIcon().iconAnchor;
    var width=marker.getIcon().iconSize.width;
    var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width,- offset.y + point.y +anchor.y)); 
    pos.apply(tooltip);
    tooltip.style.visibility="visible";
      }


// Display the map, with some controls and set the initial location 
      var mapobj = document.getElementById("map");
      var map = new GMap2(mapobj);
      map.addControl(new GLargeMapControl());
      map.addControl(new GMapTypeControl());
      map.addControl(new GScaleControl() ) ;

    var zoom=map.getBoundsZoomLevel(bounds);
    var clat = (bounds.getNorthEast().lat() + bounds.getSouthWest().lat()) /2;
    var clng = (bounds.getNorthEast().lng() + bounds.getSouthWest().lng()) /2;
    map.setCenter(new GLatLng(clat,clng),zoom,G_SATELLITE_MAP);

    map.addOverlay(polyline);
    var geoXml = new GGeoXml("<?=$fichier_kml?>");
    map.addOverlay(geoXml);

    
    start = new GMarker(<?=$start?>, starticon);
    start.tooltip = '<div class="tooltip">&nbsp;Takeoff&nbsp;</div>';
    GEvent.addListener(start, "mouseover", function() {
        showTooltip(start);
    });
    GEvent.addListener(start, "mouseout", function() {
        tooltip.style.visibility="hidden";
    });
    map.addOverlay(start);
    
    end = new GMarker(<?=$end?>, endicon);
    end.tooltip = '<div class="tooltip">&nbsp;Landing&nbsp;</div>';

    GEvent.addListener(end, "mouseover", function() {
        showTooltip(end);
    });
    GEvent.addListener(end, "mouseout", function() {
        tooltip.style.visibility="hidden";
    });
    map.addOverlay(end);
    
    
// ====== set up marker mouseover tooltip div ======
      var tooltip = document.createElement("div");
      document.getElementById("map").appendChild(tooltip);
      tooltip.style.visibility="hidden";
      

map.enableDoubleClickZoom();
 GEvent.addDomListener(mapobj, "DOMMouseScroll", GoogleMapWheelZoom);
 GEvent.addDomListener(mapobj, "mousewheel", GoogleMapWheelZoom);

    }
    
    // display a warning if the browser was not compatible
    else {
      alert("Sorry, the Google Maps API is not compatible with this browser");
    }

    //]]>
    </script>