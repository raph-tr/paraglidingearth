<?php
/****************************************************************************
*																			*
*	Author   : Asif D. Khalyani ( www.PHPAsKS.com )							*
*	Package  : Ajax Login Script											*
*	Version  : 1.0.1														*		
*	Copyright: (C) 2008 Asif D. Khalyani									*
*	Site     : www.PHPAsKS.com												*
*	Email    : asif.khalyani@msn.com										*
*																			*
*	This program is free software; you can redistribute it and/or modify	*
*	it under the terms of the GNU General Public License as published by	*
*	the Free Software Foundation; (at your option) any later version. 		*
*																			*
*	This program is distributed in the hope that it will be useful,			*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the			*
*	GNU General Public License for more details.							*
*																			*
*****************************************************************************/
?>  
<?php
if (!isset($dirPath)) $dirPath="";
include($dirPath."includes/rating_functions.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="<?php echo $dirPath;?>css/rating_style.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="<?php echo $dirPath;?>js/rating_update.js"></script>	
</head>
<body>	
	<?php
	if (isset($_GET['task_id']) or isset($taskId)) {
		if (isset($taskId)) $taskIdRate = $taskId;
		if (isset($_GET['task_id'])) $taskIdRate = $_GET['task_id'];
		/*
		USAGE:
		id = integer (number)
		show 3/5 = boolean (true/false)
		show percentage = boolean (true/false)
		show votes = boolean (true/false); 
		allow vote = 'novote' (string) OPTIONAL, if not using, leave empty or NULL
		
		pullRating(id, show 3/5, show percentage, show votes, allow vote);
		
		USAGE FOR TOP VOTES:
		id = integer (number)
		table_name = name of the table that are holding your items you are rating (string)
		table_id = the id of the field in the table you are rating. This is usually 'id' or 'article_id'
		table_title = the title of the field in the table you are rating.  This is something like 'article_title'
		*/
		echo pullRating($taskIdRate,true,false,true); 
	}
	?>

</body>
</html>