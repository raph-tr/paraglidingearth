<?php
require_once "graph/BarPlot.class.php";

include "../connexion.php";

$queryTotalFlights = "SELECT id FROM `pgsim_comp_results` WHERE competition =".$_GET['event_id'];
$resultTotalFlights= mysql_query($queryTotalFlights);
$numTotalFlights   = mysql_num_rows($resultTotalFlights);

$values[0] = $numTotalFlights;

$queryTotalFlights = "SELECT count( pgsim_comp_results.id ) AS number, is_pge_challenge AS num_task
FROM `pgsim_comp_results` 
LEFT JOIN pgsim_comp_tasks ON pgsim_comp_tasks.id = task_id
WHERE pgsim_comp_tasks.competition = ".$_GET['event_id']."
GROUP BY is_pge_challenge";
$resultTotalFlights= mysql_query($queryTotalFlights);

while ($valTotalFlights = mysql_fetch_array($resultTotalFlights)) {
   $values[$valTotalFlights['num_task']]=$valTotalFlights['number'];
}

$graph = new Graph(300, 200);
$graph->title->set('Flown/Completed tasks');

$group = new PlotGroup;
$group->setPadding(NULL, NULL, 35, NULL);
$group->setSpace(5, 5, NULL, NULL);

$group->grid->hide(TRUE);

$plot = new BarPlot($values, 1, 1, 0);
$plot->setBarColor(new Blue(25));
$plot->label->set($values);
$group->add($plot);

$queryTotalFlightsCompleted = "SELECT id FROM `pgsim_comp_results`
 WHERE competition =".$_GET['event_id']." and distance = 999999";
$resultTotalFlightsCompleted= mysql_query($queryTotalFlightsCompleted);
$numTotalFlightsCompleted   = mysql_num_rows($resultTotalFlightsCompleted);

$values[0] = $values[0] - $numTotalFlightsCompleted;

$queryTotalFlightsCompleted = "SELECT count( pgsim_comp_results.id ) AS number, is_pge_challenge AS num_task
FROM `pgsim_comp_results` 
LEFT JOIN pgsim_comp_tasks ON pgsim_comp_tasks.id = task_id
WHERE pgsim_comp_tasks.competition = ".$_GET['event_id']." and distance = 999999
GROUP BY is_pge_challenge";
$resultTotalFlightsCompleted= mysql_query($queryTotalFlightsCompleted);

while ($valTotalFlightsCompleted = mysql_fetch_array($resultTotalFlightsCompleted)) {
   $values[$valTotalFlightsCompleted['num_task']]=$values[$valTotalFlightsCompleted['num_task']] - $valTotalFlightsCompleted['number'];
}

$plot = new BarPlot($values, 1, 1, 0);
$plot->setBarColor(new Red(25));
$plot->label->set($values);
$plot->xAxis->setLabelText(array('a', 'b', 'c', 'd', 'e', 'f'));

$group->add($plot);

$graph->add($group);
$graph->draw();
?>