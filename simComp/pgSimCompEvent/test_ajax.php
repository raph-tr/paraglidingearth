<html>
<head>
<script src="ajax.js" type="text/javascript"></script>
</head>
<body>
<script language="javascript">
	function ajaxSaveToDb(element)
	{ 
		var req =  createAjaxInstance();
		var input1 = element[0];
		var input2 = element[1];
		var data = "input1=" + input1 + "&input2=" + input2;

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					alert(""+req.responseText+"");	
				}	
				else	
				{
					alert("Error: returned status code " + req.status + " " + req.statusText);
				}	
			} 
		}; 
        
		req.open("POST", "test_ajax_php.php", true); 
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(data); 
	} 
</script>


<a href="#" onclick="javascript:ajaxSaveToDb(['element1','element2'])" >Submit</a>
</body>
</html>