<?php
session_start();
include "../connexion.php";
include "library.php";

 $show_connected=0;
 include "../pgearth/compteur.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>Welcome to Comp module !</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>

<h1>Welcome to pgSim Competition Events</h1>

<?php
$here = "index"; 
include "tabs_header.php";
?>
<div class="rubriqueSite">

&nbsp;<br />
<p style="text-align:center; width:100%; background-color:#F8ECE0"><em>Is my browser compatible with "pgSim Competition Events" ?<br /><a href="http://www.paraglidingforum.com/viewtopic.php?t=31566" target="_blank">Answer the poll and read results here ! 
</a></em></p>

<h2>What are the pgSim competition events ?</h2>
The pgSim competition offers to everyone to fly and be ranked on tasks that other users built.<br />
But the big (and always increasing) number of available tasks may make the competition less addictive as you can not compete 'head to head' with the other pilots.<br />
That is why we imagined this 'competition events' mode.
<br />
The competition events are made to be just like real life pg competitions :<br />
 - <b>events are limited in time</b> : each task has a starting and an ending date,<br />
 - a pilot can <b>only make one try to fly each task</b>,<br />
 - a <b>ranking for each task and for the whole event</b> is calculated after each task is finished.

<h2>Who can participate ?</h2>
Everyone ! Just fly the task while it is open !

<h2>How are the events designed ? Who can build events and tasks ?</h2>
For the moment, events and tasks are made by the guy at paraglidingearth.
But if you want to create a new event, <a href="http://www.paraglidingforum.com/viewtopic.php?t=31004" target="_blank" >please let me know it here</a>. 

<br />
&nbsp;
</div>


<div class="rubriqueSite">
<?php

$qEvent = "select * from pgsim_comp_events where id<>0";
$rEvent = mysql_query($qEvent);
$nEvent = mysql_num_rows($rEvent);
echo "<h2>".$nEvent." events</h2>
<ul>";

while ($vEvent=mysql_fetch_array($rEvent)){

$qNum = "select id from pgsim_comp_tasks where competition = '".$vEvent['id']."'";
$rNum = mysql_query($qNum);
$num  = mysql_num_rows($rNum);

$isOpen='finished';
$actualTime = time();
$qOpen = "select starts from pgsim_comp_tasks where competition = '".$vEvent['id']."' order by starts asc limit 1";
$rOpen = mysql_query($qOpen);
$vOpen = mysql_fetch_array($rOpen);
$opens= strtotime($vOpen['starts']);

$qClose = "select expires from pgsim_comp_tasks where competition = '".$vEvent['id']."' order by expires desc limit 1";
$rClose = mysql_query($qClose);
$vClose = mysql_fetch_array($rClose);
$closes= strtotime($vClose['expires']);

if ( $actualTime < $opens ) $isOpen='<b>not open yet</b>';
if ( $actualTime > $opens and $actualTime < $closes ) $isOpen='<b>open</b>';
?>

<li><a href="events.php?event=<?php echo $vEvent['id'];?>"><b><?php echo $vEvent['event_name'];?></b></a>
 - <?php echo $num;?> tasks 
 - <?php if($isOpen=='<b>open</b>') echo '<img src="../pgearth/images/famfamfamicons/tick.png" /> '; else echo '<img src="../pgearth/images/famfamfamicons/cross.png" /> '; echo $isOpen;?>
 - 
<?php if ($isOpen=='<b>not open yet</b>') echo "<b>";?> Starting : <?php echo date("r e", $opens);?><?php if ($isOpen=='<b>not open yet</b>') echo "</b>";?> ,
<?php if ($isOpen=='<b>open</b>') echo "<b>";?>  ending : <?php echo date("r e", $closes);?><?php if ($isOpen=='<b>open</b>') echo "</b>";?> 
<br />
<em><?php echo $vEvent['event_comment']; ?></em>
</li>
<?php
}
?>
</ul>
</div>
</body>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1083947-1";
urchinTracker();
olLoaded=1;
</script>

</html>
