<?php
session_start();
include "../connexion.php";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script type="text/javascript" src="../scripts/overlib.js"></script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>pgSim Competition Events</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="../scripts/moodalbox/js/mootools.js"></script>
<script type="text/javascript" src="../scripts/moodalbox/js/moodalbox.js"></script>
<link rel="stylesheet" href="../scripts/moodalbox/css/moodalbox.css" type="text/css" media="screen" />
</head>
<body>

<h1>Welcome to pgSim Competition Events</h1> 
 
<?php
$here = "help"; 
include "tabs_header.php";
?>
<h3>
Keyboard controls
</h3>
<h2>
Game control :
</h2>
<ul>
 <li>'p' : pause   // not yet implemented</li>
 <li>'t' : show/hide thermals   // not yet implemented</li>
</ul>
<h2>
Glider handling :
</h2>
<ul>
 <li> 'Space Bar' : run forward while on ground (=launch)</li>
     <li> 'left arrow' : turn left</li>
     <li> 'right arrow' : turn right</li>
     <li> 'up arrow' : accelerate</li>
     <li> 'down arrow' : decelerate</li>
 <li> 'b' : B Stall      </li>
 <li> 'r' : throw rescue (irreversible)      </li>
</ul>
<h2>
Camera control : 
</h2>
<ul>
 <li> z : zoom out</li>
 <li> a : zoom in</li>
 <li> s : turn head to the left</li>
 <li> d : turn head to the right</li>
 <li> x : look down</li>
 <li> e : look up </li>
 <li> c : turn head backwards</li>
</ul>
<h2>
Instruments
</h2>
<ul>
 <li> v : turn vario sound on/off</li>
</ul>
<h3>
Things you should know
</h3>
<ul>
 <li>the sim may be unstable, mostly while loading : be patient and wait for a few minutes (even after the sims says it is loaded) if you don't have the glider and the clouds. Don't hesitate to refresh the page (F5 key) before taking off if everything is not loaded correctly. This may take a certain number of tries :(</li>
 <li>experience shows that the sim is more stable and less subject to crash with firefox or chrome browsers rather than internet explorer. (this is only a purely rough statistical statement)</li>
 <li>at takeoff, fly straight ahead, and wait a few seconds for the vario to 'wake up', this usually avoids having problem beeing stuck in mountain at take off</li>
 <li>you only have one try per task, but flights shorter than 10 minutes can be reseted to give the pilot another try : <a href="http://www.paraglidingforum.com/viewtopic.php?p=221732#221732" target="_blank">see here for details</a></li>
 <li>if the sim crashes or freezes while flying, go back to the task results page, wait for one minute and you will have a "resume your flight" link next to your nickname.</li>
 <li><a href="http://www.paraglidingforum.com/viewtopic.php?t=18805" target="_blank">Discuss about this sim on ParaglidingForum</a></li>
 <li>If glider deasapears when airborn, then you need a more recent version of the googleearth plugin, reinstall a newer version</li>
 <li>Thermals drift with wind : look for them upwind of clouds </li>
 <li>while using the mouse and buttons, if you leave the button without having released the click, the button will be locked. To unlock, move back to the button and click once</li>
</ul>

    </div>
</div>

</body>
</html>
