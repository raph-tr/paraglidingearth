<html>
	<head>
		<!--
			<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
		-->
		<style>
		#contentFly {
			display: none;
			max-width: 742px;
			overflow: scroll;
		}
		#timeRange, #colorLine, #timeLabels {
			width: 100%;
		}
		#colorLine, .colorDash {
			height: 13px;
		}
		.colorDash {
			display: inline-block;
		}
		#colorLine {
			top: 0px;
			position: relative;
			z-index: 0;
			left: 0px;
		}
		#timeLine{
			width: 100%;
			margin-bottom: 12px;
		}
		#timeLabels{
			position: relative;
			top: 12px;
		}
		.timeLabel {
			position: absolute;
			top: 8px;
			transform: rotate(-90deg);
			transform-origin: left top 0;
			font-size: x-small;
			font-weight: bold;
		}
		#tableForecast{
			text-align: center;
			border: solid 1px lightgray;
		}
		td, th {
			padding: 0px 3px;
			border: solid 1px darkkhaki;
			font-size: smaller;
		}
		.vText{
			transform: rotate(-90deg);
			transform-origin: center center;
			font-size: x-small;
			font-weight: bold;
			max-width: 20px;
		}
		.flyable { color: green; }
		.unflyable { color: red; }
		</style>
	</head>
	<body>
		<div id="loadingFly">
			<i class="fas fa-circle-notch fa-spin"></i> loading...
		</div>
		
		<div id="siteNameFly"></div>
		<div id="contentFly">
			<h4>Option 1 : timeline</h4>
			<div id="timeLine">
				<div id="colorLine"></div>
				<input id="timeRange" type="range">
				<div id="timeLabels"></div>
			</div>
			<div id="reasonIcon"></div>
			<div id="rangeDate"></div>

			<div id="flyability"></div>
			<div id="reason"></div>

			<div id="rangeForecast">
				<div id="windSpeed"></div>
				<div id="windDir"></div>
				<div id="rain"></div>
				<div id="cloudbase"></div>
			</div>
			<hr />
			<h4>Option 2 : table</h4>
			<table id="tableForecast">
				<tr id="daysRow" style="height:47px"><td> </td></tr>
				<tr id="windSpeedRow"><td><i class='fa fa-wind'></i> (m/s)</td></tr>
				<tr id="windDirRow"><td><i class='far fa-compass'></i> </td></tr>
				<tr id="rainRow"><td><i class='fas fa-cloud-rain'></i> rain</td></tr>
				<tr id="cloudbaseRow"><td><i class='fas fa-smog'></i> cloudbase</td></tr>
			</table>
		</div>
		
		<!--<script src="../assets/js/localhost/jquery-2.1.4.min.js"></script>-->
		<script>
			site = <?php echo $_GET['site'];?>;
			dates = [];
			// days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
			previousDay = -12; // dummy day
			i = 0;
			now = new Date();
			searchNow = true;
			
			// we query the forecasts :
			$.getJSON( "../assets/ajax/flyable/forecasts/"+site+".json", function( data ) {
				
				if ( true /*data['orientations'] != "00000000"*/ ) {
					
					forecasts = data['forecasts'];
					nbForecasts = Object.keys(forecasts).length;
					console.log(nbForecasts);
					
					$("#loadingFly").css("display","none");
					$("#siteNameFly").html(data['site'] + "<img src='../assets/img/windrose/26/"+site+".png' />");
					$("#contentFly").css("display","block");
					$("#colorLine, #timeLabels").html("");
					dates=[];
					
					$.each( forecasts, function( date, forecast ) {
						
						dates.push(date);	// keep each date in an array 
						d = new Date(date);	// get a JS date
						
						currentForecast = forecasts[date];
						
						// where to put the range cursor ??
						if (searchNow && now < d) {
							nowIndex = i;
							showTextForecast( date );
							searchNow=false;
						}
						
					
						// show range labels for days
						currentDay = d.getDay();
						if (currentDay != previousDay) {
							$("#timeLabels").append("<span class='timeLabel' id='timeLabel"+i+"'>"+d.toLocaleString([],{weekday:'short'})+"</span>");
							$("#timeLabel"+i).css("left", (parseInt(100*i/(nbForecasts-0.5)))+"%");
						}
						
						
						// Build colored line
						$("<span id='dash"+i+"' class='colorDash'></span>").appendTo("#colorLine");
						if (forecasts[date]['flyability'] == 100 ) $("#dash"+i).css("background","green");
						else $("#dash"+i).css("background","red");
						
						
						// Option 2 : make a table
						if (currentForecast['flyability'] == 100) flyClass = "flyable";
						else flyClass = "unflyable";
						if (currentDay != previousDay) $("#daysRow").append("<td class='vText "+flyClass+"'>"+d.toLocaleString([],{weekday:'short'})+"</td>");
						else $("#daysRow").append("<td class='vText "+flyClass+"'>"+d.toLocaleTimeString([],{hour:'2-digit', minute:'2-digit'})+"</td>");
						if (currentForecast['reason'].includes("windSpeed") ) $("#windSpeedRow").append("<td class='unflyable'>"+currentForecast['windSpeed']+"</td>");
						else $("#windSpeedRow").append("<td class='flyable'>"+currentForecast['windSpeed']+"</td>");
						if (currentForecast['reason'].includes("orientationsUnknown") ) $("#windDirRow").append("<td id='dirArrow"+i+"' style='color:orange'><i class='fas fa-question'></i></td>");
						else {
							if (currentForecast['reason'].includes("windDir") ) $("#windDirRow").append("<td id='dirArrow"+i+"' class='unflyable'><i class='fa fa-long-arrow-alt-down'></i></td>");
							else $("#windDirRow").append("<td id='dirArrow"+i+"' class='flyable'><i class='fa fa-long-arrow-alt-down'></i></td>");
						}
						$("#dirArrow"+i).css("transform","rotate("+currentForecast['windDir']+"deg)");
						if (currentForecast['rain'] == 0 ) $("#rainRow").append("<td class='flyable'></td>");
						else $("#rainRow").append("<td class='unflyable'><i class='fas fa-cloud-rain'></i></td>");
						if (currentForecast['reason'].includes("cloudbase") ) $("#cloudbaseRow").append("<td class='unflyable'><i class='fas fa-smog'></i> "+Math.round(currentForecast['cloudbase'])+"</td>");
						else {
							textCb = Math.round(currentForecast['cloudbase']) == 0 ? "" : "<i class='fas fa-smog'></i> "+Math.round(currentForecast['cloudbase']);
							$("#cloudbaseRow").append("<td class='flyable'>"+textCb+"</td>");
						}
						
						previousDay = currentDay;
						i++;
					});
					
					
					
					//range of the range input
					document.getElementById("timeRange").max = dates.length - 1;
					document.getElementById("timeRange").value = nowIndex;
					$(".colorDash").css("width", 100/(dates.length-1)+"%");
					$("#dash0").css("width", 50/(dates.length-1)+"%");
					//lastDash = dates.length-1
					$("#dash"+ (dates.length-1)).css("width", 50/(dates.length-1)+"%");
					
					//display the information as selected date changes
					$("#timeRange").on("change mousemove", function() {
						thisDate = dates[$(this).val()];
						showTextForecast( thisDate );
						
					});
				} else {
					$("#siteNameFly").html(data['site']+" : no site orientations known..  :(");
					$("#loadingFly").css("display","none");
				}
			})
			.fail(function() {
				$.get( "../assets/ajax/flyable/import_weather.php?site="+site, function() {
					$("#contentFly").html("Problem loadinf the weather data, try moving to anothr tab, then back here...");
				});
			});
			
			function showTextForecast( thisDate ) {
					thisDateDate = new Date( thisDate );
					
					dateOptions = options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute:'2-digit' };
					$("#rangeDate").html( thisDateDate.toLocaleString([],options) );
					$("#flyability").html("flyability : "+forecasts[thisDate]['flyability']+"%");
					if (forecasts[thisDate]['flyability'] < 100 ) $("#reason").html("reason : "+forecasts[thisDate]['reason']+"");
					else $("#reason").html("it seems fine");
					$("#windSpeed").html("<i class='fas fa-wind'></i> wind : "+forecasts[thisDate]['windSpeed']+" m/s");
					$("#windDir").html("<i class='fas fa-compass'></i> windDir : "+forecasts[thisDate]['windDir']+"°");
					$("#rain").html("<i class='fas fa-cloud-showers-heavy'></i> rain : "+forecasts[thisDate]['rain']+"");
					$("#cloudbase").html("<i class='fas fa-smog'></i> cloudbase : "+forecasts[thisDate]['cloudbase']+" m");
					
					if (forecasts[thisDate]['flyability'] == 100 ) {
						$("#flyability, #reason").addClass("flyable").removeClass("unflyable");
						$("#reasonIcon").html('<i class="far fa-thumbs-up flyable"></i>');
					} else { 
						$("#flyability, #reason").addClass("unflyable").removeClass("flyable");;
						$("#reasonIcon").html("");
					}
						if (forecasts[thisDate]['reason'].includes("windSpeed")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-wind" title="'+forecasts[thisDate]["windSpeed"]+'"></i>');
							$("#windSpeed").addClass("unflyable").removeClass("flyable"); 
						} else { $("#windSpeed").addClass("flyable").removeClass("unflyable"); }
						if (forecasts[thisDate]['reason'].includes("windDir")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-compass"></i>');
							$("#windDir").addClass("unflyable").removeClass("flyable"); 
						} else { $("#windDir").addClass("flyable").removeClass("unflyable"); }
						if (forecasts[thisDate]['reason'].includes("rain")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-cloud-rain"></i>');
							$("#rain").addClass("unflyable").removeClass("flyable");
						} else { $("#rain").addClass("flyable").removeClass("unflyable"); }
						if (forecasts[thisDate]['reason'].includes("cloudbase")) {
							$("#reasonIcon").append(' <i class="unflyable fas fa-smog"></i>');
							$("#cloudbase").addClass("unflyable").removeClass("flyable");
						} else { $("#cloudbase").addClass("flyable").removeClass("unflyable"); }
			}
			
		</script>
	</body>
</html>
