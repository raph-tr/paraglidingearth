<!--
<div id="splashModal" class="modal fade in">
    <div class="inner-modal">
        <div class="modal-container">
            <p>PAraglidingEArth is back on tracks (but still not fully featured)</p>
            <button id="splashModalClose" class="close" data-dismiss="modal" data-target="#splashModal">Ok, got it !</button>
        </div>
    </div>  
</div>
-->

    <div class="modal fade" id="splashModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
				Welcome on PgEarth !
            </h4>
          </div>  <!-- /.modal-header -->
		  <div class="modal-body">
			  <div class="alert alert-warning">
				<p>
					Welcome on PgEarth, a collaborative worlwide database of free flight sites !
					<ul>
						<li>responsive, adaptative design : will look nice and eficient on all (not too small) screens,</li>
						<li>edit information right on place: login and click any site item to modify it smooth as the butterfly floats !</li>
						<li>clever (?) worldwide sites smart loading: as fast as a bee can load a map</li>
						<li>Filter Sites by various criteria : country, wind directions, kinds of flights...</li>
						<li>... and more to come ?</li>
					</ul>
				</p>
				<p>
					This beeing said, the website is still under permanent construction/maintenance
					and bugs shall be fixed as fast as the little time i got for it will allow me to... :(
				</p>
				<p>
					Enjoy your visit anyway !!!
				</p>
			</div>
			<div class="alert alert-danger">
				<p>The data provided on this site is for informational and planning purposes only.</p>
				<p>Absolutely no accuracy or completeness guarantee is implied or intended. All information on this website is for informational purpose only and must not be used and trusted 'as is'. Please understand that it may be outdated, unclear, or simply wrong !</p>
				<p>We hope you are aware that you can not trust anything you read on the internet: the website owner can not be held responsible for any decision taken on the basis of the information presented here : please always consider getting fresh and official legal information from the flying sites local people, clubs and/or authorities and consider the weather carefully before you go fly !</p>
				<p>This beeing said, fly happy and safe ! ;)</p>
			</div>
          </div><!-- /.modal-body -->
          <div class="modal-footer">
			<button id="splashModalClose" class="btn btn-default" data-dismiss="modal" data-target="#splashModal">Ok, got it, don't show me this again !</button>
          </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
