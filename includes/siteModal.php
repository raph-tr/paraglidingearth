<!------ SITE MODAL  -------->

    <div class="modal fade" id="siteDetailsModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
	   <div id="wrap">  
        <div class="modal-content" id="site-modal-content">
          <div class="modal-header">
			<?php if($currentUser->role_id >= 2) { ?>
				<div class="alert-warning">
					<a id="deleteSiteAdminLink" href="#" target="_blank" onclick="return confirm('Are you REALLY sure ??\n(last chance to cancel..)');"><i class="fa fa-trash"></i> Delete this site</a><br />
					<a id="closedSiteLink" 		href="#" target="_blank" onclick="return confirm('Are you sure ?');"><i class="fa fa-times"></i> Site is CLOSED !</a>
				</div>	
			<?php }	 ?>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-title row" id="modal-site-title"></div>
          </div><!-- /.modal-header -->
          <div class="modal-body" id="modal-site-body">
            <ul class="nav nav-tabs" id="siteBody">
				<li id="modal-site-description-tab" class="active"><a href="#modal-site-body-description" data-toggle="tab"><i class="fa fa-file-text-o"></i><span class=" hidden-xs">&nbsp;Description</span></a></li>
				<li id="modal-site-map-tab"><a href="#modal-site-body-map" data-toggle="tab"><i class="fa fa-map-marker"></i><span class=" hidden-xs">&nbsp;Map</span></a></li>
				<li id="modal-site-pictures-tab"><a href="#modal-site-body-pictures" data-toggle="tab"><i class="fa fa-camera-retro"></i><span class=" hidden-xs">&nbsp;Pictures</span></a></li>
				<li id="modal-site-flyable-tab"><a href="#modal-site-body-flyable" data-toggle="tab"><i class="fas fa-cloud-sun-rain"></i><span class=" hidden-xs">&nbsp;Does it fly?</span></a></li>
				<li id="modal-site-weather-tab"><a href="#modal-site-body-weather" data-toggle="tab"><i class="fa fa-cloud"></i><span class=" hidden-xs">&nbsp;Weather</span></a></li>
				<li id="modal-site-users-tab"><a href="#modal-site-body-users" data-toggle="tab"><i class="fa fa-user"></i><span class=" hidden-xs">&nbsp;Who's here ?</span></a></li>
				<li id="modal-site-history-tab"><a href="#modal-site-body-history" data-toggle="tab"><i class="fa fa-calendar-o"></i><span class=" hidden-xs">&nbsp;History</span></a></li>
				<li id="modal-site-flights-tab"><a href="#modal-site-body-flights" data-toggle="tab"><i class="fa fa-plane"></i><span class=" hidden-xs">&nbsp;Flights</span></a></li>
			</ul>
			<div class="tab-content" id="siteBodyContent">
				<div class="tab-content" id="siteDescription">
				
				<div class="tab-pane fade active in" id="modal-site-body-description">  
					<i class="fa fa-circle-o-notch fa-spin"></i> Site text description   
	            </div>
	              
	            <div class="tab-pane fade" id="modal-site-body-pictures">
					<ul class="first">
					   <li><i class="fa fa-circle-o-notch fa-spin"></i> Site pictures here</li>
					</ul>
 	            </div><!-- /#modal-site-body-pictures -->
	            
	            <div class="tab-pane fade" id="modal-site-body-weather">
	            	<!--<div id="siteWeatherIframe"></div>-->
	            	<iframe width="90%" height="480" id="windyIFrame" frameborder="0"></iframe>
	            	<!-- <div id="windy" style="width:90%; height:460px"></div> -->
					<p>Courtesy of <a href="http://windy.com" target="_blank">windy.com</a></p>
 	            </div><!-- /#modal-site-body-weather -->
 	            
  	            <div class="tab-pane fade" id="modal-site-body-map">
					<div class="row">
						<div id="siteMapText" class="col-sm-6" ></div>
						<div id="siteMapDirection" class="col-sm-6" ></div>
					</div>	
					<div id="siteMapIframe"></div>
 	            </div><!-- /#modal-site-body-map -->
  				
  				<div class="tab-pane fade" id="modal-site-body-users">
					Site users here
				</div><!-- /#modal-site-body-users -->
  				
  				<div class="tab-pane fade" id="modal-site-body-history">
					As far as we know, this site file was contributed to by :
					<li id="history"></li>
					Thank you to them :)
				</div><!-- /#modal-site-body-users -->
 	    
 	    		
				<div class="tab-pane fade" id="modal-site-body-flights">
					<div  id="modal-site-body-flights-leonardo"><i class="fa fa-circle-o-notch fa-spin"></i> Leonardo</div>
					<hr />
					<div  id="modal-site-body-flights-xcontest"><i class="fa fa-circle-o-notch fa-spin"></i> XContest</div>
					<hr />
					<div  id="modal-site-body-flights-sim"><i class="fa fa-circle-o-notch fa-spin"></i> Fly the Pg Sim</div>
				</div><!-- /#modal-site-body-users -->
				
				
				<div class="tab-pane fade" id="modal-site-body-flyable">
					<div id="loadingFly">
						<i class="fas fa-circle-notch fa-spin"></i> loading...
					</div>
					
					<div id="siteNameFly"></div>
					<div id="contentFly">
						<h4>Option 1 : timeline</h4>
						<div id="timeLine">
							<input id="timeRange" type="range">
							<div id="colorLine"></div>
							<div id="timeLabels"></div>
						</div>
						<div id="reasonIcon"></div>
						<div id="rangeDate"></div>

						<div id="flyability"></div>
						<div id="reason"></div>

						<div id="rangeForecast">
							<div id="windSpeed"></div>
							<div id="windDir"></div>
							<div id="rain"></div>
							<div id="cloudbase"></div>
						</div>
						<hr />
						<h4>Option 2 : table</h4>
						<table id="tableForecast">
							<tr id="daysRow" style="height:47px"></tr>
							<tr id="windSpeedRow"></tr>
							<tr id="windDirRow"></tr>
							<tr id="rainRow"></tr>
							<tr id="cloudbaseRow"></tr>
						</table>
					</div>
				</div><!-- /#modal-site-body-flyable -->
				
	          </div><!-- /#siteDescription -->
            </div><!-- /.tab-content -->
          </div><!-- /.modal-body -->
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
       </div><!-- / #wrap -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<!------ END SITE MODAL  -------->
