<html>
  <head>
   	<style>
  		#windy {
  			width: 100%;
  			height: 100%;
  		}
  	</style>
   <script src="https://unpkg.com/leaflet@0.7.7/dist/leaflet.js"></script>
   <script src="https://api4.windy.com/assets/libBoot.js"></script>
  </head>
  <body>
    <div id="windy"></div>

   <script>

   	getWindyMap(<?php echo $_GET['lat']; ?>, <?php echo $_GET['lng']; ?>);

    function getWindyMap(lat, lng) {

        var wind= [];
        var windyLatCenter = lat;
        var windyLngCenter = lng;
      
        const options = {
                    // Required: API key
                    key: '3vdBtb72vicQnIeP7pu7xbe0jrTALSPL',
                    lat: windyLatCenter, lon: windyLngCenter, zoom: 11,
        }


        windyInit( options, windyAPI => {

            const { picker, utils, overlays, broadcast } = windyAPI
            overlays.wind.setMetric('m/s')

            // Wait since weather is rendered
            broadcast.once('redrawFinished', () => {

                picker.open({ lat: windyLatCenter, lon: windyLngCenter })
                // Opening of a picker (async)

            })


           picker.on('pickerOpened', latLon => {
                  // picker has been opened at latLon coords
                  var weatherParams = picker.getParams()
                  wind = getWind(weatherParams)

     
                 broadcast.on('paramsChanged', params => {

                    console.log('Params changed:', params )

                    picker.open()
                    var weather = picker.getParams()
                    console.log(weather)
                    var readable = getWind(weather)
                  //  console.log( readable )

                })

            });

            function getWind( weather ) {

              var wSpeed = Math.sqrt ( weather.values[0] * weather.values[0] + weather.values[1] * weather.values[1] );
              var wAngle = Math.atan2 ( weather.values[1], weather.values[0] );
              wAngle =   270 - 180*wAngle/Math.PI;
              if(wAngle > 360) wAngle-=360;

              return [wSpeed, wAngle];
            }

        });
  }

    </script>

  </body>
</html>