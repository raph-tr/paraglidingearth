<?php

while ($val=mysqli_fetch_array($result)){
	
	if ($val['name']) {
		
		/* we store results in an array instead of processing them on the fly
		 * so that we can then sort the array by distance from a given point
		 * and limit to closest results
		 * unfortunately this part of the code is not written here...
		 * (and that makes that array story totaly useless...)
		 */
		 
		// let's get last edit date...
		$qLastEdit = "SELECT date FROM `sites_users` WHERE `item_type` LIKE 'site' AND `item_id` = ".$val['id']." ORDER BY `sites_users`.`date` DESC LIMIT 1 ";
		$rLastEdit = mysqli_query($bdd, $qLastEdit);
		$vLastEdit = mysqli_fetch_array($rLastEdit);
		$last_edit[$count] =  $vLastEdit['date'];
		
		 
		$name[$count] = $val['name'];
		$id_site[$count] = $val['id'];
		$iso[$count] = $val['iso'];

		$lng[$count] = $val['lng'];
		$lat[$count] = $val['lat'];
		$takeoff_altitude[$count] = $val['takeoff_altitude'];

		$landing_name[$count] = $val['landing_name'];
		$landing_lat[$count]  = $val['landing_lat'];
		$landing_lng[$count]  = $val['landing_lng'];
		$landing_altitude[$count] = $val['landing_altitude'];

		$landing_description[$count] = $val['landing'];
		$takeoff_description[$count] = $val['takeoff'];

		$N[$count] = $val['N'];
		$NE[$count] = $val['NE'];
		$E[$count] = $val['E'];
		$SE[$count] = $val['SE'];
		$S[$count] = $val['S'];
		$SW[$count] = $val['SW'];
		$W[$count] = $val['W'];
		$NW[$count] = $val['NW'];

		$hike[$count] = $val['hike'];
		$soaring[$count] = $val['soaring'];
		$thermal[$count] = $val['thermal'];
		$xc[$count] = $val['xc'];
		$flatland[$count] = $val['flatland'];
		$winch[$count] = $val['winch'];
		$hanggliding[$count] = $val['hanggliding'];

		$takeoff_going_there[$count] = $val['access'];
		$takeoff_flight_rules[$count] = $val['rules'];
		$takeoff_contacts[$count] = $val['contact'];
		$takeoff_comments[$count] = $val['comments'];
		$weather[$count] = $val['weather'];

		$web[$count] = $val['web'];


		if (isset($_GET['distance'])) {
			$latRad = 3.14159265 * $val['lat']   / 180;
			$lngRad = 3.14159265 * $val['lng']   / 180;
			$distance[$count] = round(6366 * acos(cos($latRad) * cos($latref) * cos($lngref-$lngRad) + sin($latRad) * sin($latref)),2);
		}

		$count++;
	}
}

$limit= count($name);

if (isset ($_GET['limit'])) {
	$limit = min ($_GET['limit'], count ($name));
}

if (isset($_GET['distance']) and $limit > 0) {
	array_multisort( $distance, SORT_ASC,
		$name, $iso, $id_site, $last_edit,
		$lat, $lng, $takeoff_altitude, 
		$landing_name, $landing_lat, $landing_lng, $landing_altitude, 
		$takeoff_description, $landing_description,
		$hike, $soaring, $thermal, $xc ,$flatland,$winch, $hanggliding,  
		$takeoff_comments, $weather, $takeoff_going_there, $takeoff_flight_rules, $takeoff_contacts,
		$web,
		$N,$NE,$E,$SE,$S,$SW,$W,$NW
	);
}



$search = new SimpleXMLElement("<search></search>");

$search->addChild('results', $limit);

for ($k=0;$k<$limit;$k++){

	$takeoff = $search->addChild('takeoff');
	$takeoff->addChild('name'); 
		$takeoff->name = $name[$k];
	if (isset($_GET['distance'])){
		$takeoff->addChild('distance', $distance[$k]);
	}
	$takeoff->addChild('countryCode', $iso[$k]);
	$takeoff->addChild('takeoff_altitude', $takeoff_altitude[$k]);
	$takeoff->addChild('takeoff_description');
		$takeoff->takeoff_description = $takeoff_description[$k];
	$takeoff->addChild('lat', $lat[$k]);
	$takeoff->addChild('lng', $lng[$k]);
	$takeoff->addChild('paragliding', '1');
	$takeoff->addChild('hanggliding', $hanggliding[$k]);
	$takeoff->addChild('hike', $hike[$k]);
	$takeoff->addChild('thermals', $thermal[$k]);
	$takeoff->addChild('soaring', $soaring[$k]);
	$takeoff->addChild('xc', $xc[$k]);
	$takeoff->addChild('flatland', $flatland[$k]);
	$takeoff->addChild('winch', $winch[$k]);
	$takeoff->addChild('pge_site_id', $id_site[$k]);
	$takeoff->addChild('pge_link', 'http://www.paraglidingearth.com/?site='.$id_site[$k]);
	$takeoff->addChild('last_edit', $last_edit[$k]);
	$takeoff->addChild('orientations');
		$takeoff->orientations->addChild('N', $N[$k]);
		$takeoff->orientations->addChild('NE', $NE[$k]);
		$takeoff->orientations->addChild('E', $E[$k]);
		$takeoff->orientations->addChild('SE', $SE[$k]);
		$takeoff->orientations->addChild('S', $S[$k]);
		$takeoff->orientations->addChild('SW', $SW[$k]);
		$takeoff->orientations->addChild('W', $W[$k]);
		$takeoff->orientations->addChild('NW', $NW[$k]);

	if ( $style=='detailled') {
		
		// TEXT FIELDS
		$takeoff->addChild('flight_rules');
			$takeoff->flight_rules = $takeoff_flight_rules[$k];
		$takeoff->addChild('going_there');
			$takeoff->going_there = $takeoff_going_there[$k];
		$takeoff->addChild('comments');
			$takeoff->comments = $takeoff_comments[$k];
		$takeoff->addChild('weather');
			$takeoff->weather = $weather[$k];
		// END OF TEXT FIELDS
				
		// TAKEOFF PARKING WHEN STYLE=DETAILLED 
		$queryTakeoffParkingDetails = "select
						takeoff_parking_lat, takeoff_parking_lng, takeoff_parking_description
						from site where id = ".$id_site[$k] ;
		$resultTakeoffParkingDetails = mysqli_query($bdd, $queryTakeoffParkingDetails);
		$valTakeoffParkingDetails = mysqli_fetch_array($resultTakeoffParkingDetails);

		if ( $valTakeoffParkingDetails['takeoff_parking_lat']!=0 or $valTakeoffParkingDetails['takeoff_parking_lng']!=0 or $valTakeoffParkingDetails['takeoff_parking_description']!="" ) {
			$takeoff->addChild('takeoff_parking');
				$takeoff->takeoff_parking->addChild('takeoff_parking_lat', $valTakeoffParkingDetails['takeoff_parking_lat']);
				$takeoff->takeoff_parking->addChild('takeoff_parking_lng', $valTakeoffParkingDetails['takeoff_parking_lng']);
				$takeoff->takeoff_parking->addChild('takeoff_parking_description');
					$takeoff->takeoff_parking->takeoff_parking_description = $valTakeoffParkingDetails['takeoff_parking_description'];
		}
		// END OF TAKEOFF PARKING

		// ALTERNATE TAKEOFFS
		$queryAlternateTakeoffsDetails = "SELECT
						lat, lng, description, name, altitude
						FROM site_extra_items 
						WHERE type LIKE 'takeoff' AND site = ".$id_site[$k] ;
		$resultAlternateTakeoffsDetails = mysqli_query($bdd, $queryAlternateTakeoffsDetails);
		$alt_takeoffs = $takeoff->addChild('alternate_takeoffs');
		while ($valAlternateTakeoffsDetails = mysqli_fetch_array($resultAlternateTakeoffsDetails)) {
			if ( $valAlternateTakeoffsDetails['lat']!=0 or $valAlternateTakeoffsDetails['lng']!=0 or $valAlternateTakeoffsDetails['description']!="" ) {
				$alt_takeoff = $alt_takeoffs->addChild('alternate_takeoff');
					$alt_takeoff->addChild('name');
						$alt_takeoff->name = $valAlternateTakeoffsDetails['name'];
					$alt_takeoff->addChild('lat', $valAlternateTakeoffsDetails['lat']);
					$alt_takeoff->addChild('lng', $valAlternateTakeoffsDetails['lng']);
					$alt_takeoff->addChild('altitude', $valAlternateTakeoffsDetails['altitude']);
					$alt_takeoff->addChild('description');
						$alt_takeoff->description = $valAlternateTakeoffsDetails['description'];
			}
		}
		// END OF ALTERNATE TAKEOFFS

		
		// MAIN LANDING
		$hasAMainLanding = false;
		if($landing_lng[$k] != 0 and $landing_lat[$k] != 0){
			$hasAMainLanding = true;
			$landing = $search->addChild('landing');
			$landing->addChild('site'); 
				$landing->site = $name[$k];
			$landing->addChild('landing_name'); 
				$landing->landing_name = $landing_name[$k];
			$landing->addChild('landing_countryCode', $iso[$k]);
			$landing->addChild('landing_lat', $landing_lat[$k]);
			$landing->addChild('landing_lng', $landing_lng[$k]);
			$landing->addChild('landing_altitude', $landing_altitude[$k]);
			$landing->addChild('landing_description');
				$landing->landing_description = $landing_description[$k];
			$landing->addChild('landing_pge_site_id', $id_site[$k]);
		}
		// END OF MAIN LANDING

		// LANDING PARKING
		$queryLandingParkingDetails = "select
						landing_parking_lat, landing_parking_lng, landing_parking_description
						from site where id = ".$id_site[$k] ;
		$resultLandingParkingDetails = mysqli_query($bdd, $queryLandingParkingDetails);
		$valLandingParkingDetails = mysqli_fetch_array($resultLandingParkingDetails);
		if ( $valLandingParkingDetails['landing_parking_lat']!=0 or $valLandingParkingDetails['landing_parking_lng']!=0 or $valLandingParkingDetails['landing_parking_description']!="" ) {
			$landing->addChild('landing_parking');
			$landing->landing_parking->addChild('landing_parking_lat', $valLandingParkingDetails['landing_parking_lat']);
			$landing->landing_parking->addChild('landing_parking_lng', $valLandingParkingDetails['landing_parking_lng']);
			$landing->landing_parking->addChild('landing_parking_description');
				$landing->landing_parking->landing_parking_description = $valLandingParkingDetails['landing_parking_description'];
		}
		// END OF LANDING PARKING
		
		
		// ALTERNATE LANDINGS
		$queryAlternateLandingsDetails = "SELECT
						lat, lng, description, name, altitude
						FROM site_extra_items 
						WHERE type LIKE 'landing' AND site = ".$id_site[$k] ;						
		$resultAlternateLandingsDetails = mysqli_query($bdd, $queryAlternateLandingsDetails);		
		if ($hasAMainLanding) {
			$alt_landings = $landing->addChild('alternate_landings');
			while ($valAlternateLandingsDetails = mysqli_fetch_array($resultAlternateLandingsDetails)) {
				if ( $valAlternateLandingsDetails['lat']!=0 or $valAlternateLandingsDetails['lng']!=0 or $valAlternateLandingsDetails['description']!="" ) {
					$alt_landing = $alt_landings->addChild('alternate_landing');
						$alt_landing->addChild('name');
							$alt_landing->name = $valAlternateLandingsDetails['name'];
						$alt_landing->addChild('lat', $valAlternateLandingsDetails['lat']);
						$alt_landing->addChild('lng', $valAlternateLandingsDetails['lng']);
						$alt_landing->addChild('altitude', $valAlternateLandingsDetails['altitude']);
						$alt_landing->addChild('description');
							$alt_landing->description = $valAlternateLandingsDetails['description'];
				}
			}
		}
		// END OF ALTERNATE LANDINGS

				
		// ALTERNATE PARKINGS
		// ... to be done....
	}
}

Header('Content-type: text/xml');
echo $search->asXML();
?>
